import 'package:cleanis/config/colors.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class ReviewsEdit extends StatefulWidget {
  final String id, storeId;
  const ReviewsEdit({Key key, this.id, this.storeId}) : super(key: key);

  @override
  _ReviewsEditState createState() => _ReviewsEditState();
}

class _ReviewsEditState extends State<ReviewsEdit> {
  final db = FirebaseFirestore.instance;

  final currentUser = FirebaseAuth.instance.currentUser;
  String ratingValue;
  bool loading = false;

  Future loadReview() async {
    final review = await FirebaseFirestore.instance
        .collection('stores')
        .doc(widget.storeId)
        .collection('reviewers')
        .doc(widget.id)
        .get();
    return review;
  }

  Future updateReviews({String comment, String rate}) async {
    return await db
        .collection('stores')
        .doc(widget.storeId)
        .collection('reviewers')
        .doc(widget.id)
        .update({'comment': comment, 'rate': rate});
  }

  Future setRatingStore() async {
    var storeRate = [];
    var rateResult = [];
    final reviews = await db
        .collection('stores')
        .doc(widget.storeId)
        .collection('reviewers')
        .get();

    if (reviews.docs.isNotEmpty) {
      for (var element in reviews.docs) {
        storeRate.add({"rate": element.get('rate')});
      }
      var sum = (storeRate
              .map<double>((m) => double.parse(m["rate"]))
              .reduce((a, b) => a + b)) /
          reviews.docs.length;

      rateResult.add({
        "average": sum,
      });
    }
    return await db
        .collection('stores')
        .doc(widget.storeId)
        .update({'rate': rateResult[0]['average'].toStringAsFixed(1)});
  }

  Future _loadReview;

  @override
  void initState() {
    super.initState();
    _loadReview = loadReview();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: const Text('Rate and Reviews'),
      ),
      body: FutureBuilder(
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            final reviews = snapshot.data;
            final commentController = TextEditingController()
              ..text = reviews['comment'];
            return Container(
              padding: const EdgeInsets.all(15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'Rating',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  RatingBar.builder(
                    unratedColor: Colors.grey[400],
                    initialRating: double.parse(reviews['rate']),
                    minRating: 0,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,
                    itemPadding: const EdgeInsets.fromLTRB(0, 5, 5, 10),
                    itemBuilder: (context, _) => const Icon(
                      Icons.star,
                      color: Colors.amberAccent,
                    ),
                    onRatingUpdate: (rating) {
                      setState(() {
                        ratingValue = rating.toString();
                      });
                    },
                  ),
                  const Text(
                    'Comment',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(vertical: 5),
                    color: Colors.white,
                    child: TextFormField(
                      validator: (val) =>
                          val.isEmpty ? 'Fill with your comment here' : null,
                      controller: commentController,
                      keyboardType: TextInputType.multiline,
                      minLines: 2,
                      maxLines: 5,
                      decoration: const InputDecoration(
                        fillColor: Colors.red,
                        alignLabelWithHint: true,
                        border: OutlineInputBorder(),
                        hintText: 'Your message or comment',
                      ),
                    ),
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: ElevatedButton(
                          child: const Text(
                            'BATAL',
                            style: TextStyle(
                              fontSize: 14,
                              color: primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          style: ElevatedButton.styleFrom(
                            elevation: 0,
                            primary: Colors.white,
                            side: const BorderSide(
                              width: 1.5,
                              color: primaryColor,
                            ),
                          ),
                          onPressed: () {
                            setState(() {});
                          },
                        ),
                      ),
                      const SizedBox(width: 10),
                      Expanded(
                        child: ElevatedButton(
                          child: const Text('KIRIM'),
                          style: ElevatedButton.styleFrom(
                            elevation: 0,
                            primary: primaryColor,
                            textStyle: const TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          onPressed: () {
                            updateReviews(
                                comment: commentController.text,
                                rate: ratingValue);
                            setRatingStore();
                            Navigator.pop(context);
                          },
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            );
          }
          return const Center(child: CircularProgressIndicator());
        },
        future: _loadReview,
      ),
    );
  }
}
