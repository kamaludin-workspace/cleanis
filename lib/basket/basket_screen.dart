import 'package:cleanis/config/colors.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'widgets/statuscard_widget.dart';

class BasketScreen extends StatefulWidget {
  const BasketScreen({Key key}) : super(key: key);

  @override
  _BasketScreenState createState() => _BasketScreenState();
}

class _BasketScreenState extends State<BasketScreen>
    with SingleTickerProviderStateMixin {
  final nominal = NumberFormat("#,##0", "en_US");
  final uid = FirebaseAuth.instance.currentUser.uid;
  TabController _tabController;
  ScrollController _scrollViewController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2);
    _scrollViewController = ScrollController(initialScrollOffset: 0.0);
  }

  @override
  void dispose() {
    _tabController.dispose();
    _scrollViewController.dispose();
    super.dispose();
  }

  Future<QuerySnapshot> processTransactions() async {
    return FirebaseFirestore.instance
        .collection('transactions')
        .where('delivery', isEqualTo: false)
        .where('userId', isEqualTo: uid)
        .orderBy('updated_at', descending: true)
        .get();
  }

  Future<QuerySnapshot> finishedTransactions() async {
    return FirebaseFirestore.instance
        .collection('transactions')
        .where('delivery', isEqualTo: true)
        .where('userId', isEqualTo: uid)
        .orderBy('updated_at', descending: true)
        .get();
  }

  getStatusName(confirmed, picked, process, shipped, delivery) {
    if (confirmed && picked && process && shipped && delivery) {
      return 'Delivery';
    } else if (confirmed && picked && process && shipped) {
      return 'Shipped';
    } else if (confirmed && picked && process) {
      return 'On Process';
    } else if (confirmed && picked) {
      return 'Picked Up';
    } else if (confirmed) {
      return 'Confirmed';
    } else {
      return 'Menunggu konfirmasi';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: background,
      body: NestedScrollView(
        controller: _scrollViewController,
        headerSliverBuilder: (BuildContext context, bool boxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              backgroundColor: primaryColor,
              title: const Center(child: Text('My Basket')),
              pinned: true,
              floating: true,
              forceElevated: boxIsScrolled,
              bottom: TabBar(
                indicatorColor: Colors.white,
                tabs: const <Widget>[
                  Tab(text: "In Progress"),
                  Tab(text: "Completed")
                ],
                controller: _tabController,
              ),
            )
          ];
        },
        body: TabBarView(
          children: <Widget>[
            FutureBuilder(
              future: processTransactions(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (!snapshot.hasData || snapshot.data.docs.isEmpty) {
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/images/basket.png',
                          scale: 5,
                        ),
                        const SizedBox(height: 16),
                        const Text('Data transaksi kosong',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ))
                      ],
                    );
                  } else {
                    final transactions = snapshot.data.docs;
                    return MediaQuery.removePadding(
                      context: context,
                      removeTop: true,
                      child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: snapshot.data.docs.length,
                        itemBuilder: (BuildContext context, int index) {
                          String totalPrice = nominal.format((double.parse(
                                      transactions[index]['totalOrder']) *
                                  double.parse(transactions[index]['price']) +
                              (double.parse(transactions[index]['distance']) *
                                      0.001) *
                                  int.parse(
                                      transactions[index]['costDelivery'])));
                          String status = getStatusName(
                              transactions[index]['confirmed'],
                              transactions[index]['picked'],
                              transactions[index]['process'],
                              transactions[index]['shipped'],
                              transactions[index]['delivery']);
                          return StatusCard(
                            storeId: snapshot.data.docs[index]['storeId'],
                            transactionId: snapshot.data.docs[index].id,
                            store: snapshot.data.docs[index]
                                ['orderServiceName'],
                            dateOrder: transactions[index]['dateOrder'],
                            status: status,
                            price: totalPrice,
                            confirmed: transactions[index]['confirmed'],
                            picked: transactions[index]['picked'],
                            process: transactions[index]['process'],
                            shipped: transactions[index]['shipped'],
                            delivery: transactions[index]['delivery'],
                          );
                        },
                      ),
                    );
                  }
                }
                return const Center(child: CircularProgressIndicator());
              },
            ),
            // Finished Transactions
            FutureBuilder(
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (!snapshot.hasData || snapshot.data.docs.isEmpty) {
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          'assets/images/basket.png',
                          scale: 5,
                        ),
                        const SizedBox(height: 16),
                        const Text('Data transaksi kosong',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ))
                      ],
                    );
                  } else {
                    final transactions = snapshot.data.docs;
                    return MediaQuery.removePadding(
                      context: context,
                      removeTop: true,
                      child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: snapshot.data.docs.length,
                        itemBuilder: (BuildContext context, int index) {
                          String totalPrice = nominal.format((double.parse(
                                      transactions[index]['totalOrder']) *
                                  double.parse(transactions[index]['price']) +
                              (double.parse(transactions[index]['distance']) *
                                      0.001) *
                                  int.parse(
                                      transactions[index]['costDelivery'])));
                          String status = getStatusName(
                              transactions[index]['confirmed'],
                              transactions[index]['picked'],
                              transactions[index]['process'],
                              transactions[index]['shipped'],
                              transactions[index]['delivery']);
                          return StatusCard(
                            storeId: snapshot.data.docs[index]['storeId'],
                            transactionId: snapshot.data.docs[index].id,
                            store: snapshot.data.docs[index]
                                ['orderServiceName'],
                            dateOrder: transactions[index]['dateOrder'],
                            status: status,
                            price: totalPrice,
                            confirmed: transactions[index]['confirmed'],
                            picked: transactions[index]['picked'],
                            process: transactions[index]['process'],
                            shipped: transactions[index]['shipped'],
                            delivery: transactions[index]['delivery'],
                          );
                        },
                      ),
                    );
                  }
                }
                return const Center(child: CircularProgressIndicator());
              },
              future: finishedTransactions(),
            ),
          ],
          controller: _tabController,
        ),
      ),
    );
  }
}
