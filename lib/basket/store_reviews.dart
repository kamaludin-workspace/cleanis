import 'dart:math';

import 'package:cleanis/config/colors.dart';
import 'package:cleanis/loading.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class StoreReviews extends StatefulWidget {
  final String storeDocID, transactionId;
  const StoreReviews({Key key, this.storeDocID, this.transactionId})
      : super(key: key);

  @override
  _StoreReviewsState createState() => _StoreReviewsState();
}

class _StoreReviewsState extends State<StoreReviews> {
  final db = FirebaseFirestore.instance;
  final _formKey = GlobalKey<FormState>();
  final commentController = TextEditingController();
  final currentUser = FirebaseAuth.instance.currentUser;
  String ratingValue;
  bool loading = false;

  // Create reviews
  Future createReviews(
      {String docId, String comment, String dateReviews, String rate}) {
    try {
      return db
          .collection('stores')
          .doc(widget.storeDocID)
          .collection('reviewers')
          .doc(docId)
          .set({
        'comment': comment,
        'dateReviews': DateTime.now(),
        'displayName': currentUser.displayName,
        'photoUrl': currentUser.photoURL ??
            'https://ik.imagekit.io/n0t5masg5jg/stores/photoUsers/user_Bp0tCB1Pd.png?ik-sdk-version=javascript-1.4.3&updatedAt=1642516788106',
        'rate': rate,
        'userID': currentUser.uid,
        'transactionId': widget.transactionId
      });
    } catch (e) {
      return e.code;
    }
  }

  Future updateTransactions({String docId}) async {
    return await db
        .collection('transactions')
        .doc(widget.transactionId)
        .update({'isReviewed': true, 'reviewedID': docId});
  }

  // Update rate store
  Future setRatingStore() async {
    var storeRate = [];
    var rateResult = [];
    final reviews = await db
        .collection('stores')
        .doc(widget.storeDocID)
        .collection('reviewers')
        .get();

    if (reviews.docs.isNotEmpty) {
      for (var element in reviews.docs) {
        storeRate.add({"rate": element.get('rate')});
      }
      var sum = (storeRate
              .map<double>((m) => double.parse(m["rate"]))
              .reduce((a, b) => a + b)) /
          reviews.docs.length;

      rateResult.add({
        "average": sum,
      });
    }
    return await db
        .collection('stores')
        .doc(widget.storeDocID)
        .update({'rate': rateResult[0]['average'].toStringAsFixed(1)});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: primaryColor,
          title: const Text('Rate and Reviews'),
        ),
        body: loading
            ? const Loading()
            : Container(
                padding: const EdgeInsets.all(15),
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'Rating',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      RatingBar.builder(
                        unratedColor: Colors.grey[400],
                        initialRating: 0,
                        minRating: 0,
                        direction: Axis.horizontal,
                        allowHalfRating: true,
                        itemCount: 5,
                        itemPadding: const EdgeInsets.fromLTRB(0, 5, 5, 10),
                        itemBuilder: (context, _) => const Icon(
                          Icons.star,
                          color: Colors.amberAccent,
                        ),
                        onRatingUpdate: (rating) {
                          setState(() {
                            ratingValue = rating.toString();
                          });
                        },
                      ),
                      const Text(
                        'Comment',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(vertical: 5),
                        color: Colors.white,
                        child: TextFormField(
                          validator: (val) => val.isEmpty
                              ? 'Fill with your comment here'
                              : null,
                          controller: commentController,
                          keyboardType: TextInputType.multiline,
                          minLines: 2,
                          maxLines: 5,
                          decoration: const InputDecoration(
                            fillColor: Colors.red,
                            alignLabelWithHint: true,
                            border: OutlineInputBorder(),
                            hintText: 'Your message or comment',
                          ),
                        ),
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: ElevatedButton(
                              child: const Text(
                                'BATAL',
                                style: TextStyle(
                                  fontSize: 14,
                                  color: primaryColor,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              style: ElevatedButton.styleFrom(
                                elevation: 0,
                                primary: Colors.white,
                                side: const BorderSide(
                                  width: 1.5,
                                  color: primaryColor,
                                ),
                              ),
                              onPressed: () {
                                setState(() {});
                              },
                            ),
                          ),
                          const SizedBox(width: 10),
                          Expanded(
                            child: ElevatedButton(
                              child: const Text('KIRIM'),
                              style: ElevatedButton.styleFrom(
                                elevation: 0,
                                primary: primaryColor,
                                textStyle: const TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              onPressed: () {
                                const _chars =
                                    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
                                Random _rnd = Random();
                                String getRandomString(int length) =>
                                    String.fromCharCodes(Iterable.generate(
                                        length,
                                        (_) => _chars.codeUnitAt(
                                            _rnd.nextInt(_chars.length))));
                                String docId = getRandomString(20);
                                if (_formKey.currentState.validate()) {
                                  setState(() {
                                    loading = true;
                                  });
                                  ratingValue ??= '0.0';
                                  createReviews(
                                      docId: docId,
                                      comment: commentController.text,
                                      rate: ratingValue);
                                  updateTransactions(docId: docId);
                                  setRatingStore();
                                  Future.delayed(
                                      const Duration(milliseconds: 600), () {
                                    Navigator.pop(context);
                                  });
                                }
                              },
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ));
  }
}
