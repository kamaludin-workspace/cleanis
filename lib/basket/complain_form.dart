import 'package:cleanis/config/colors.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../loading.dart';

class ComplainForm extends StatefulWidget {
  final String transactionId;
  const ComplainForm({key, this.transactionId}) : super(key: key);

  @override
  State<ComplainForm> createState() => _ComplainFormState();
}

class _ComplainFormState extends State<ComplainForm> {
  final db = FirebaseFirestore.instance;
  final _formKey = GlobalKey<FormState>();
  final complaintController = TextEditingController();
  bool loading = false;

  Future updateTransactions({String message}) {
    try {
      return db
          .collection('transactions')
          .doc(widget.transactionId)
          .update({'isComplain': true, 'customerComplain': message});
    } catch (e) {
      return e.code;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: const Text('Komplain Pesanan'),
      ),
      body: loading
          ? const Loading()
          : Container(
              padding: const EdgeInsets.all(15),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'Pesan komplain anda',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 5),
                      color: Colors.white,
                      child: TextFormField(
                        validator: (val) =>
                            val.isEmpty ? 'Fill with your message here' : null,
                        keyboardType: TextInputType.multiline,
                        controller: complaintController,
                        minLines: 2,
                        maxLines: 5,
                        decoration: const InputDecoration(
                          fillColor: Colors.red,
                          alignLabelWithHint: true,
                          border: OutlineInputBorder(),
                          hintText: 'Tulis pesan komplain disini',
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: ElevatedButton(
                            child: const Text(
                              'BATAL',
                              style: TextStyle(
                                fontSize: 14,
                                color: primaryColor,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            style: ElevatedButton.styleFrom(
                              elevation: 0,
                              primary: Colors.white,
                              side: const BorderSide(
                                width: 1.5,
                                color: primaryColor,
                              ),
                            ),
                            onPressed: () {
                              setState(() {});
                            },
                          ),
                        ),
                        const SizedBox(width: 10),
                        Expanded(
                          child: ElevatedButton(
                            child: const Text('KIRIM'),
                            style: ElevatedButton.styleFrom(
                              elevation: 0,
                              primary: primaryColor,
                              textStyle: const TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            onPressed: () {
                              if (_formKey.currentState.validate()) {
                                setState(() {
                                  loading = true;
                                });
                                updateTransactions(
                                    message: complaintController.text);
                                Future.delayed(
                                    const Duration(milliseconds: 600), () {
                                  Navigator.pop(context);
                                });
                              }
                            },
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
    );
  }
}
