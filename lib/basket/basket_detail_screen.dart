import 'package:cleanis/basket/complain_form.dart';
import 'package:cleanis/config/colors.dart';
import 'package:cleanis/config/styles.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import 'reviews_edit.dart';
import 'store_reviews.dart';
import 'widgets/emptybasket_widget.dart';
import 'widgets/orderstatus_widget.dart';
import 'widgets/serviceinfo_widget.dart';
import 'widgets/store_widget.dart';
import 'widgets/storeactions_widget.dart';

class DetailBasketScreen extends StatefulWidget {
  final String transactionId, storeId;
  const DetailBasketScreen({Key key, this.transactionId, this.storeId})
      : super(key: key);

  @override
  _DetailBasketScreenState createState() => _DetailBasketScreenState();
}

class _DetailBasketScreenState extends State<DetailBasketScreen> {
  final nominal = NumberFormat("#,##0", "en_US");

  Future getTransactionDetail() async {
    final transaction = await FirebaseFirestore.instance
        .collection('transactions')
        .doc(widget.transactionId)
        .get();
    return transaction;
  }

  Future loadStore() async {
    final store = await FirebaseFirestore.instance
        .collection('stores')
        .doc(widget.storeId)
        .get();
    return store;
  }

  Future updateDelivery() async {
    var currentDate = DateTime.now();
    var date = DateFormat("d MMMM yyyy", "id_ID").format(currentDate);
    var time = DateFormat("hh.mm", "id_ID").format(currentDate);
    return await FirebaseFirestore.instance
        .collection('transactions')
        .doc(widget.transactionId)
        .update({
      'delivery': true,
      'deliveryStatusDate': date,
      'deliveryStatusTime': time
    });
  }

  Future loadComment({String docId}) async {
    final review = await FirebaseFirestore.instance
        .collection('stores')
        .doc(widget.storeId)
        .collection('reviewers')
        .doc(docId)
        .get();
    return review;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: background,
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: const Text(
          'My Order',
          style: appBarStyleLight,
        ),
      ),
      body: SingleChildScrollView(
        child: FutureBuilder(
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              try {
                final serviceDetail = snapshot.data[0];
                final store = snapshot.data[1];
                return Column(
                  children: [
                    (serviceDetail['delivery'])
                        ? FutureBuilder(
                            builder: (context, snapshot) {
                              if (snapshot.connectionState ==
                                  ConnectionState.done) {
                                try {
                                  final review = snapshot.data;
                                  Timestamp t = review['dateReviews'];
                                  DateTime d = t.toDate();
                                  var time = DateFormat("d MMMM yyyy", "id_ID")
                                      .format(d);
                                  return Container(
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(8.0)),
                                      border: Border.all(
                                          width: 1, color: Colors.grey[400]),
                                    ),
                                    padding: const EdgeInsets.all(10),
                                    margin: const EdgeInsets.fromLTRB(
                                        10, 10, 10, 0),
                                    width: double.infinity,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            CircleAvatar(
                                              radius: 26,
                                              backgroundColor: Colors.grey[300],
                                              child: CircleAvatar(
                                                radius: 25,
                                                backgroundColor: Colors.white,
                                                backgroundImage: NetworkImage(
                                                    review['photoUrl']),
                                              ),
                                            ),
                                            Container(
                                              margin:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 10),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    review['displayName'],
                                                    style: const TextStyle(
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.w700),
                                                  ),
                                                  Text(
                                                    time,
                                                    style: const TextStyle(
                                                        fontSize: 12),
                                                  ),
                                                  Row(
                                                    children: [
                                                      RatingBarIndicator(
                                                        rating: double.parse(
                                                            review['rate']),
                                                        itemBuilder:
                                                            (context, index) =>
                                                                const Icon(
                                                          Icons.star,
                                                          color: Colors
                                                              .amberAccent,
                                                        ),
                                                        itemCount: 5,
                                                        itemSize: 18,
                                                        direction:
                                                            Axis.horizontal,
                                                      ),
                                                    ],
                                                  )
                                                ],
                                              ),
                                            ),
                                            const Spacer(),
                                            ElevatedButton(
                                              style: ElevatedButton.styleFrom(
                                                tapTargetSize:
                                                    MaterialTapTargetSize
                                                        .shrinkWrap,
                                                minimumSize: Size.zero,
                                                padding: EdgeInsets.zero,
                                                shape: const CircleBorder(),
                                              ),
                                              child: Ink(
                                                decoration: BoxDecoration(
                                                    gradient: LinearGradient(
                                                      begin: Alignment.topLeft,
                                                      end: Alignment
                                                          .bottomCenter,
                                                      colors: [
                                                        Colors.blue[200],
                                                        Colors.blue[300],
                                                        Colors.blue[500],
                                                        Colors.blue[500],
                                                      ],
                                                      stops: const [
                                                        0.1,
                                                        0.3,
                                                        0.9,
                                                        1.0
                                                      ],
                                                    ),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20)),
                                                child: const SizedBox(
                                                  height: 40,
                                                  width: 40,
                                                  child: Icon(
                                                    Icons.edit,
                                                    size: 18,
                                                  ),
                                                ),
                                              ),
                                              onPressed: () {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        ReviewsEdit(
                                                      id: serviceDetail[
                                                          'reviewedID'],
                                                      storeId: serviceDetail[
                                                          'storeId'],
                                                    ),
                                                  ),
                                                ).then(
                                                    (_) => {setState(() {})});
                                              },
                                            ),
                                          ],
                                        ),
                                        const Divider(),
                                        const SizedBox(height: 5),
                                        Text(review['comment']),
                                      ],
                                    ),
                                  );
                                } catch (e) {
                                  return Container(
                                    margin: const EdgeInsets.fromLTRB(
                                        10, 10, 10, 0),
                                    padding: const EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(8.0)),
                                      border: Border.all(
                                          width: 1, color: Colors.grey[400]),
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Expanded(
                                          flex: 2,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              const Text(
                                                'This transaction has been done, give rate and comment for share',
                                                style: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                              ElevatedButton(
                                                child: const Text(
                                                    'Rated and Reviews'),
                                                style: ElevatedButton.styleFrom(
                                                  elevation: 0,
                                                  primary: primaryColor,
                                                  textStyle: const TextStyle(
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                                onPressed: () {
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (context) =>
                                                          StoreReviews(
                                                        storeDocID:
                                                            serviceDetail[
                                                                'storeId'],
                                                        transactionId:
                                                            serviceDetail.id,
                                                      ),
                                                    ),
                                                  ).then(
                                                      (_) => {setState(() {})});
                                                },
                                              )
                                            ],
                                          ),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Image.asset(
                                            'assets/images/reviews.png',
                                            scale: 3,
                                          ),
                                        )
                                      ],
                                    ),
                                  );
                                }
                              }
                              return const Center(
                                child: CircularProgressIndicator(),
                              );
                            },
                            future:
                                loadComment(docId: serviceDetail['reviewedID']),
                          )
                        : const SizedBox(),
                    Container(
                      margin: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(8.0)),
                        border: Border.all(width: 1, color: Colors.grey[400]),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          // extract widget
                          Container(
                            padding: const EdgeInsets.all(15.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  serviceDetail['orderServiceName'],
                                  style: const TextStyle(
                                    fontSize: 22,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                Text(serviceDetail['dateOrder']),
                              ],
                            ),
                          ),
                          const Divider(),
                          Container(
                            padding: const EdgeInsets.symmetric(horizontal: 15),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                const Text(
                                  'Order Info',
                                  style: TextStyle(
                                    fontSize: 18,
                                  ),
                                ),
                                Text(
                                  '#' + serviceDetail['transactionId'],
                                  style: const TextStyle(
                                      fontWeight: FontWeight.w700),
                                )
                              ],
                            ),
                          ),
                          const Divider(),
                          ServiceInfo(
                            title: 'Waktu Jemput',
                            subtitle: (serviceDetail['pickupDate'] != null)
                                ? '${serviceDetail['pickupDate']}, ${serviceDetail['pickupTime']}'
                                : 'Kosong',
                          ),
                          ServiceInfo(
                            title: 'Waktu Antar',
                            subtitle: (serviceDetail['deliveryDate'] != null)
                                ? '${serviceDetail['deliveryDate']}, ${serviceDetail['deliveryTime']}'
                                : 'Kosong',
                          ),
                          ServiceInfo(
                            title: 'Alamat - ${serviceDetail['addressName']}',
                            subtitle: serviceDetail['address'],
                          ),
                          ServiceInfo(
                            title: 'Metode Pembayaran',
                            subtitle: serviceDetail['paymentMethod'],
                          ),
                          ServiceInfo(
                              title:
                                  'Rp. ${nominal.format(int.parse(serviceDetail['price']))} X ${serviceDetail['totalOrder']} ${serviceDetail['orderType']}',
                              subtitle:
                                  'Rp. ${nominal.format(int.parse(serviceDetail['price']) * double.parse(serviceDetail['totalOrder']))}'),
                          ServiceInfo(
                              title:
                                  'Rp. ${nominal.format(double.parse(serviceDetail['costDelivery']))}/Km X 200 M',
                              subtitle:
                                  'Rp. ${((double.parse(serviceDetail['distance']) * 0.001) * double.parse(serviceDetail['costDelivery'])).toStringAsFixed(0)}'),
                          const Divider(),
                          ServiceInfo(
                              title: 'Total',
                              subtitle:
                                  'Rp. ${nominal.format((double.parse(serviceDetail['totalOrder']) * int.parse(serviceDetail['price'])) + (double.parse(serviceDetail['distance']) * 0.001) * int.parse(serviceDetail['costDelivery']))}'),
                          Container(
                            margin: const EdgeInsets.only(top: 7),
                            child: ExpansionTile(
                              title: const Text('Order Status'),
                              children: [
                                Container(
                                  padding:
                                      const EdgeInsets.fromLTRB(12, 0, 15, 0),
                                  child: Column(
                                    children: [
                                      // Confirmed
                                      OrderStatus(
                                        isActive: serviceDetail['confirmed'],
                                        title: 'Confirmed',
                                        subtile: (serviceDetail['confirmed'])
                                            ? 'Your order has been confirmed'
                                            : 'Your order has not been confirmed',
                                        time: serviceDetail[
                                                'confirmStatusDate'] ??
                                            '',
                                        date: serviceDetail[
                                                'confirmStatusTime'] ??
                                            '',
                                        image: 'confirmed.png',
                                      ),
                                      OrderStatus(
                                        isActive: serviceDetail['picked'],
                                        title: 'Picked',
                                        subtile: (serviceDetail['picked'])
                                            ? 'Your order has been picked'
                                            : 'Your order has not been picked',
                                        time: serviceDetail['pickStatusDate'] ??
                                            '',
                                        date: serviceDetail['pickStatusTime'] ??
                                            '',
                                        image: 'picked.png',
                                      ),
                                      OrderStatus(
                                        isActive: serviceDetail['process'],
                                        title: 'In process',
                                        subtile: (serviceDetail['process'])
                                            ? 'Your order now is on process'
                                            : 'Your order has not been process',
                                        time: serviceDetail[
                                                'processStatusDate'] ??
                                            '',
                                        date: serviceDetail[
                                                'processStatusTime'] ??
                                            '',
                                        image: 'process.png',
                                      ),
                                      OrderStatus(
                                        isActive: serviceDetail['shipped'],
                                        title: 'Shipped',
                                        subtile: (serviceDetail['shipped'])
                                            ? 'Your order is out for shipped'
                                            : 'Your order has not been shipeed',
                                        time:
                                            serviceDetail['shippStatusDate'] ??
                                                '',
                                        date:
                                            serviceDetail['shippStatusTime'] ??
                                                '',
                                        image: 'shipped.png',
                                      ),
                                      OrderStatus(
                                        isActive: serviceDetail['delivery'],
                                        title: 'Delivered',
                                        subtile: (serviceDetail['delivery'])
                                            ? 'Successfully delivered on you'
                                            : 'Your order has not been delivery',
                                        time: serviceDetail[
                                                'deliverStatusDate'] ??
                                            '',
                                        date: serviceDetail[
                                                'deliverStatusTime'] ??
                                            '',
                                        image: 'delivered.png',
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          (serviceDetail['shipped'] &&
                                  !serviceDetail['delivery'])
                              ? Container(
                                  padding: const EdgeInsets.all(15),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          const Expanded(
                                            child: Text(
                                              'Konfirmasi diterima sebelum 12 Januari 2022',
                                              softWrap: true,
                                              maxLines: 3,
                                            ),
                                          ),
                                          const SizedBox(width: 5),
                                          Expanded(
                                            child: ElevatedButton(
                                              child: const Text(
                                                  'Pesanan Diterima'),
                                              style: ElevatedButton.styleFrom(
                                                elevation: 0,
                                                primary: primaryColor,
                                                textStyle: const TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                              onPressed: () {
                                                updateDelivery();
                                                setState(() {});
                                              },
                                            ),
                                          )
                                        ],
                                      ),
                                      (!serviceDetail['isComplain'] &&
                                              serviceDetail[
                                                      'customerComplain'] ==
                                                  '')
                                          ? Container(
                                              width: double.infinity,
                                              alignment: Alignment.center,
                                              child: const Text(
                                                'ATAU',
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              margin:
                                                  const EdgeInsets.symmetric(
                                                vertical: 10,
                                              ),
                                            )
                                          : const SizedBox(),
                                      (!serviceDetail['isComplain'] &&
                                              serviceDetail[
                                                      'customerComplain'] ==
                                                  '')
                                          ? ElevatedButton(
                                              child:
                                                  const Text('Ajukan Komplain'),
                                              style: ElevatedButton.styleFrom(
                                                minimumSize:
                                                    const Size.fromHeight(40),
                                                elevation: 0,
                                                primary: Colors.red[600],
                                                textStyle: const TextStyle(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                              onPressed: () {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        ComplainForm(
                                                      transactionId:
                                                          serviceDetail.id,
                                                    ),
                                                  ),
                                                ).then(
                                                    (_) => {setState(() {})});
                                              },
                                            )
                                          : const SizedBox(
                                              height: 10,
                                            ),
                                      (serviceDetail['isComplain'] &&
                                              serviceDetail[
                                                      'customerComplain'] !=
                                                  null)
                                          ? Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                const Text('Komplain:',
                                                    style: TextStyle(
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    )),
                                                Text(serviceDetail[
                                                    'customerComplain']),
                                                const Text('Respon Komplain:',
                                                    style: TextStyle(
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    )),
                                                Text(serviceDetail[
                                                    'storeComplain']),
                                              ],
                                            )
                                          : const SizedBox(),
                                      (!serviceDetail['isComplain'] &&
                                              serviceDetail[
                                                      'customerComplain'] !=
                                                  '')
                                          ? Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                const Text('Komplain:',
                                                    style: TextStyle(
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    )),
                                                Text(serviceDetail[
                                                    'customerComplain']),
                                                const Text('Respon Komplain:',
                                                    style: TextStyle(
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    )),
                                                Text(serviceDetail[
                                                    'storeComplain']),
                                              ],
                                            )
                                          : const SizedBox(),
                                    ],
                                  ),
                                )
                              : const SizedBox(),
                        ],
                      ),
                    ),
                    StoreActions(
                      onPressed: () {
                        if (store['numberPhone'] != null) {
                          launch('tel:' +
                              ((store['numberPhone'] == null)
                                  ? '0'
                                  : store['numberPhone']));
                        }
                      },
                      title: 'Message the dobby store',
                      subtitle: 'Text him if you have any question',
                      image: 'whatsapp.png',
                    ),
                    StoreActions(
                      onPressed: () {
                        if (store['numberPhone'] != null) {
                          launch('sms:' +
                              ((store['numberPhone'] == null)
                                  ? '0'
                                  : store['numberPhone']));
                        }
                      },
                      title: 'Call the dobby store',
                      subtitle: 'Call him if you have any question',
                      image: 'phone.png',
                    ),
                    StoreActions(
                      onPressed: () {
                        if (store['lat'] != null) {
                          String originLat = serviceDetail['lat'];
                          String originLng = serviceDetail['lng'];
                          String destLat = store['lat'];
                          String destLng = store['lng'];
                          String url =
                              'https://www.google.com/maps/dir/?api=1&origin=$originLat,$originLng&destination=$destLat,$destLng&travelmode=driving&dir_action=navigate';
                          launch(url);
                        }
                      },
                      title: 'Navigations',
                      subtitle: 'Get route to the store',
                      image: 'route.png',
                    ),
                    Store(
                      storeId: store.id,
                      store: store['name'],
                      addres: store['address'],
                      star: store['rate'],
                      image: store['thumbnail'] ?? '',
                    ),
                  ],
                );
              } catch (e) {
                return EmptyBasket(
                  text: 'Error occured e $e',
                );
              }
            }
            return const Center(child: CircularProgressIndicator());
          },
          future: Future.wait([
            getTransactionDetail(),
            loadStore(),
          ]),
        ),
      ),
    );
  }
}
