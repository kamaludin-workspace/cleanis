import 'package:flutter/material.dart';

class EmptyBasket extends StatelessWidget {
  final String text;
  const EmptyBasket({
    Key key,
    this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        const SizedBox(height: 120),
        Image.asset(
          'assets/images/basket.png',
          scale: 3,
        ),
        const SizedBox(height: 20),
        Text(
          text,
          style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
        const Text(
          'Track your progress here',
          style: TextStyle(fontSize: 17, fontWeight: FontWeight.normal),
        )
      ],
    );
  }
}
