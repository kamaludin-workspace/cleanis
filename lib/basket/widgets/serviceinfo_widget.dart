import 'package:flutter/material.dart';

class ServiceInfo extends StatelessWidget {
  final String title, subtitle;
  const ServiceInfo({
    Key key,
    this.title,
    this.subtitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Row(
        children: [
          Text(title),
          const Spacer(),
          Text(subtitle),
        ],
      ),
    );
  }
}
