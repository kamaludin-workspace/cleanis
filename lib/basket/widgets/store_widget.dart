import 'package:cleanis/store/store_detail_screen.dart';
import 'package:flutter/material.dart';

class Store extends StatelessWidget {
  final String storeId, store, addres, star, image;
  const Store({
    Key key,
    this.store,
    this.addres,
    this.star,
    this.image,
    this.storeId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
      height: 160,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: const BorderRadius.all(
          Radius.circular(8.0),
        ),
        border: Border.all(
          width: 1,
          color: Colors.grey[300],
        ),
      ),
      child: InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => StoreDetailScreen(firestoreDocID: storeId),
            ),
          );
        },
        child: Column(
          children: <Widget>[
            Container(
              height: 95,
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                  topRight: Radius.circular(8),
                  topLeft: Radius.circular(8),
                ),
                image: DecorationImage(
                  image: NetworkImage(image),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 12),
              decoration: BoxDecoration(
                border: Border(
                  top: BorderSide(
                    width: 0.8,
                    color: Colors.grey[300],
                  ),
                ),
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 3,
                    child: SizedBox(
                      height: 38,
                      child: RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: "$store \n",
                              style: const TextStyle(
                                fontFamily: 'Nunito',
                                color: Colors.black,
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            TextSpan(
                              text: addres,
                              style: const TextStyle(
                                fontFamily: 'Nunito',
                                color: Colors.black87,
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      height: 35,
                      alignment: Alignment.topLeft,
                      child: Row(
                        children: <Widget>[
                          const Spacer(),
                          const Icon(
                            Icons.grade,
                            size: 17,
                            color: Colors.orangeAccent,
                          ),
                          Text(
                            ' $star',
                            style: const TextStyle(
                                fontSize: 15, fontWeight: FontWeight.w600),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
