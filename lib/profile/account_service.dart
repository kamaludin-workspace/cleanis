import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AccountService {
  final db = FirebaseFirestore.instance;
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  final uid = FirebaseAuth.instance.currentUser.uid;
  CollectionReference usersProfile =
      FirebaseFirestore.instance.collection('usersProfile');

  Future<bool> isCollectionExits() async {
    QuerySnapshot<Map<String, dynamic>> _query = await db
        .collection('usersProfile')
        .doc(uid)
        .collection('address')
        .get();

    if (_query.docs.isNotEmpty) {
      return false;
    } else {
      return true;
    }
  }

  void unPrimaryAddress({String docId}) async {
    return db
        .collection('usersProfile')
        .doc(uid)
        .collection('address')
        .doc(docId)
        .update({'primary': false});
  }

  Future<String> primaryAddress() async {
    var collection = FirebaseFirestore.instance
        .collection('usersProfile')
        .doc(uid)
        .collection('address');
    var querySnapshot =
        await collection.where('primary', isEqualTo: true).get();

    var add = '';

    for (var snapshot in querySnapshot.docs) {
      Map<String, dynamic> data = snapshot.data();
      add = data['address'];
    }
    return add;
  }

  Future updatePrimary({String docId}) {
    try {
      return db
          .collection('usersProfile')
          .doc(uid)
          .collection('address')
          .doc(docId)
          .update({'primary': true});
    } catch (e) {
      return e.code;
    }
  }

  Future deleteAddress({String docId}) {
    try {
      return db
          .collection('usersProfile')
          .doc(uid)
          .collection('address')
          .doc(docId)
          .delete();
    } catch (e) {
      return e.code;
    }
  }

  Future addAddress(
      {String name, String address, double lat, double lng, bool primary}) {
    try {
      return db
          .collection('usersProfile')
          .doc(uid)
          .collection('address')
          .doc()
          .set({
        'primary': primary,
        'name': name,
        'address': address,
        'location': GeoPoint(lat, lng),
        'lat': lat,
        'lng': lng,
        'created_at': FieldValue.serverTimestamp()
      });
    } catch (e) {
      return e.code;
    }
  }
}
