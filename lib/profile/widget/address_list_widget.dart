import 'package:cleanis/config/colors.dart';
import 'package:cleanis/profile/account_service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class AddressList extends StatefulWidget {
  final String name, address, docId;
  final bool primary;

  const AddressList({
    Key key,
    this.name,
    this.address,
    this.primary,
    this.docId,
  }) : super(key: key);

  @override
  State<AddressList> createState() => _AddressListState();
}

class _AddressListState extends State<AddressList> {
  final AccountService accountService = AccountService();

  final uid = FirebaseAuth.instance.currentUser.uid;

  String idCurrentPrimaryAddress;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: const BorderRadius.all(
          Radius.circular(5.0),
        ),
        border: Border.all(
          width: 2,
          color: (widget.primary) ? Colors.blue : Colors.grey[200],
        ),
      ),
      padding: const EdgeInsets.all(15),
      width: double.infinity,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: SizedBox(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  (widget.primary)
                      ? const Text(
                          'Alamat Aktif',
                          style: TextStyle(
                            fontSize: 22,
                            color: primaryColor,
                            fontWeight: FontWeight.w900,
                          ),
                        )
                      : Container(),
                  Text(
                    widget.name,
                    style: const TextStyle(
                      fontSize: 20,
                      height: 1.3,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  const SizedBox(height: 5),
                  Text(
                    widget.address,
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.grey[600],
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  const SizedBox(height: 10),
                  (!widget.primary)
                      ? Row(
                          children: <Widget>[
                            Expanded(
                              child: OutlinedButton(
                                style: OutlinedButton.styleFrom(
                                  primary: Colors.orangeAccent,
                                  side: const BorderSide(
                                      color: Colors.orangeAccent, width: 1),
                                ),
                                child: const Text('Hapus',
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                    )),
                                onPressed: () {
                                  accountService.deleteAddress(
                                      docId: widget.docId);
                                  final snackBar = SnackBar(
                                    content: const Text('Alamat dihapus'),
                                    action: SnackBarAction(
                                      label: 'Close',
                                      onPressed: () {
                                        ScaffoldMessenger.of(context)
                                            .hideCurrentSnackBar();
                                      },
                                    ),
                                  );
                                  ScaffoldMessenger.of(context)
                                      .showSnackBar(snackBar);
                                },
                              ),
                            ),
                            const SizedBox(width: 10),
                            Expanded(
                              child: OutlinedButton(
                                style: OutlinedButton.styleFrom(
                                  primary: (widget.primary)
                                      ? Colors.black54
                                      : Colors.white,
                                  backgroundColor: (!widget.primary)
                                      ? Colors.blue
                                      : Colors.grey[100],
                                ),
                                child: Text(
                                    (widget.primary)
                                        ? 'Alamat Utama'
                                        : 'Set Utama',
                                    style: const TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                    )),
                                onPressed: () {
                                  if (!widget.primary) {
                                    var collection = FirebaseFirestore.instance
                                        .collection('usersProfile')
                                        .doc(uid)
                                        .collection('address');
                                    collection
                                        .where('primary', isEqualTo: true)
                                        .limit(1)
                                        .get()
                                        .then((QuerySnapshot snapshot) {
                                      for (var document in snapshot.docs) {
                                        idCurrentPrimaryAddress = document.id;
                                      }
                                      accountService.unPrimaryAddress(
                                          docId: idCurrentPrimaryAddress);
                                    });
                                    Future.delayed(
                                        const Duration(milliseconds: 500), () {
                                      accountService.updatePrimary(
                                          docId: widget.docId);
                                    });
                                  }
                                },
                              ),
                            ),
                          ],
                        )
                      : Container(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
