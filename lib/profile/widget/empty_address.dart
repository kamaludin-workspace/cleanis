import 'package:flutter/material.dart';

class EmptyAddres extends StatelessWidget {
  const EmptyAddres({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: MediaQuery.of(context).size.height * 0.20),
        Image.asset(
          'assets/images/address.png',
          scale: 1,
        ),
        const SizedBox(height: 20),
        const Text(
          'No address record yet',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ],
    );
  }
}
