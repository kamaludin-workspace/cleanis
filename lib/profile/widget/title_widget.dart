import 'package:flutter/material.dart';

class ProfileTitle extends StatelessWidget {
  final String title;
  const ProfileTitle({
    Key key,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
    );
  }
}
