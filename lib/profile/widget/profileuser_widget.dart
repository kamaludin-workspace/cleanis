import 'package:cleanis/config/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';

class ProfileUser extends StatelessWidget {
  final String name, location, address, image;
  const ProfileUser({
    Key key,
    this.name,
    this.location,
    this.address,
    this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 115,
      width: double.infinity,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 0.8,
            color: Colors.grey[300],
          ),
        ),
        image: const DecorationImage(
          image: ExactAssetImage('assets/images/bg_poly.jpg'),
          fit: BoxFit.cover,
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: CircularProfileAvatar(
              image,
              errorWidget: (context, url, error) => const Icon(Icons.error),
              placeHolder: (context, url) => const SizedBox(
                width: 35,
                height: 35,
                child: CircularProgressIndicator(),
              ),
              radius: 40,
              backgroundColor: primaryColor,
              borderWidth: 3,
              borderColor: Colors.grey[400],
              imageFit: BoxFit.fitHeight,
              elevation: 0,
              cacheImage: true,
              showInitialTextAbovePicture: false,
            ),
          ),
          RichText(
            text: TextSpan(
              children: [
                TextSpan(
                  text: "$name \n",
                  style: const TextStyle(
                    color: Colors.black54,
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                const WidgetSpan(
                  child: Icon(
                    Icons.location_on,
                    size: 18,
                    color: primaryColor,
                  ),
                ),
                TextSpan(
                  text: location,
                  style: const TextStyle(
                    fontWeight: FontWeight.w300,
                    color: Colors.black87,
                    height: 2,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
