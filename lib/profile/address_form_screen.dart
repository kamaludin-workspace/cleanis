import 'package:cleanis/profile/account_service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cleanis/config/colors.dart';
import 'package:geolocator/geolocator.dart';
import 'package:universe/universe.dart';

import '../loading.dart';
import 'widget/submitbutton_widget.dart';

class AddressFormScreen extends StatefulWidget {
  const AddressFormScreen({Key key}) : super(key: key);

  @override
  _AddressFormScreenState createState() => _AddressFormScreenState();
}

class _AddressFormScreenState extends State<AddressFormScreen> {
  var appBar = AppBar();
  bool loading = false;
  final uid = FirebaseAuth.instance.currentUser.uid;
  final db = FirebaseFirestore.instance;

  final _mapKey = UniqueKey();
  List<Marker> markers = [];

  Position position;

  getLocation() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.best);
    return position;
  }

  void addressInfo() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await showDialog<String>(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: const Text(
            "Menambah alamat baru?",
            style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
          ),
          content: const Text(
            "Ketik atau tap pada peta lokasi yang anda inginkan, dan isi form yang ditampilkan.\nJangan lupa, pastikan lokasi sudah benar dan presisi.",
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
          actions: <Widget>[
            ElevatedButton(
              child: const Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        ),
      );
    });
  }

  @override
  void initState() {
    super.initState();
    getLocation();
    addressInfo();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: const Text('My Address'),
        elevation: 0,
        actions: <Widget>[
          IconButton(
            padding: const EdgeInsets.only(right: 20),
            icon: const Icon(
              Icons.info_outline_rounded,
              color: Colors.white,
            ),
            onPressed: () {
              addressInfo();
            },
          ),
        ],
      ),
      body: FutureBuilder(
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return U.MapBox(
              accessToken:
                  'pk.eyJ1IjoienllbCIsImEiOiJja3ZoY3RraWI1eGE4MzFxd21paGlyZGQ5In0.-Zcr5egbG9bgHhq856uN5Q',
              key: _mapKey,
              center: [
                position.latitude ?? '1.0073318019947193',
                position.longitude ?? '102.71110037148851'
              ],
              showLocationIndicator: true,
              zoom: 15,
              markers: U.MarkerLayer(
                markers,
                onLongPress: (position, latlng) {
                  if (latlng is LatLng && mounted) {
                    setState(() {
                      markers.removeWhere((marker) => marker.latlng == latlng);
                    });
                  }
                },
              ),
              layers: [
                U.MarkerLayer(
                  markers,
                  widget: const MarkerImage('assets/images/addres_place.png'),
                  size: 48,
                ),
              ],
              onTap: (latlng) {
                if (markers.isEmpty) {
                  if (mounted) {
                    setState(() {
                      Marker marker = U.Marker(latlng, data: latlng);
                      markers.add(marker);
                    });
                  }
                } else if (markers.isNotEmpty) {
                  markers.remove(markers.first);
                  setState(() {
                    Marker marker = U.Marker(latlng, data: latlng);
                    markers.add(marker);
                  });
                }
                showModalBottomSheet(
                  context: context,
                  backgroundColor: Colors.transparent,
                  isDismissible: true,
                  isScrollControlled: true,
                  builder: (context) => AddressForm(
                      isLoading: false, lat: latlng.lat, lng: latlng.lng),
                );
              },
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
        future: getLocation(),
      ),
    );
  }
}

final _formKey = GlobalKey<FormState>();

class AddressForm extends StatelessWidget {
  final AccountService accountService = AccountService();
  final _nameController = TextEditingController()..text = 'Home';
  final _addressController = TextEditingController();

  final double lat;
  final double lng;

  final bool isLoading;

  AddressForm({key, this.isLoading, this.lat, this.lng}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15.0),
              topRight: Radius.circular(15.0),
            ),
          ),
          child: (isLoading)
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.37,
                        child: const Loading(),
                      )
                    ])
              : Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Text(
                        'Tambah Alamat',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      const Text(
                        'Pastikan pin anda sudah sesuai dengan lokasi',
                        style: TextStyle(fontSize: 14),
                      ),
                      const SizedBox(height: 25),
                      Container(
                        color: Colors.white,
                        child: TextFormField(
                          validator: (val) =>
                              val.isEmpty ? 'Name address is required' : null,
                          controller: _nameController,
                          keyboardType: TextInputType.text,
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 10.0),
                            labelText: 'Address Name',
                            hintText: 'Home',
                          ),
                        ),
                      ),
                      const SizedBox(height: 15),
                      Container(
                        color: Colors.white,
                        child: TextFormField(
                          validator: (val) =>
                              val.isEmpty ? 'Your address is required' : null,
                          controller: _addressController,
                          keyboardType: TextInputType.text,
                          decoration: const InputDecoration(
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 10.0),
                            hintText: 'Jl. xxxx',
                            border: OutlineInputBorder(),
                            labelText: 'Alamat',
                          ),
                        ),
                      ),
                      const SizedBox(height: 15),
                      ButtonSubmit(
                        onPress: () async {
                          if (_formKey.currentState.validate()) {
                            bool isAddressExits =
                                await accountService.isCollectionExits();
                            accountService.addAddress(
                              name: _nameController.text,
                              address: _addressController.text,
                              lat: lat,
                              lng: lng,
                              primary: isAddressExits,
                            );
                            int count = 0;
                            Navigator.popUntil(context, (route) {
                              return count++ == 2;
                            });
                          }
                        },
                        text: 'SUBMIT',
                      )
                    ],
                  ),
                ),
        ),
      ),
    );
  }
}
