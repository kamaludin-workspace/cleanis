import 'package:cleanis/config/colors.dart';
import 'package:cleanis/profile/address_form_screen.dart';
import 'package:cleanis/profile/widget/empty_address.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'widget/address_list_widget.dart';
import 'widget/submitbutton_widget.dart';

class AddressScreen extends StatefulWidget {
  const AddressScreen({Key key}) : super(key: key);

  @override
  _AddressScreenState createState() => _AddressScreenState();
}

class _AddressScreenState extends State<AddressScreen> {
  var appBar = AppBar();
  final uid = FirebaseAuth.instance.currentUser.uid;
  final db = FirebaseFirestore.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: background,
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: const Text('My Address'),
        elevation: 0,
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.add),
            tooltip: 'New Address',
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const AddressFormScreen(),
                ),
              );
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
        child: Column(children: [
          StreamBuilder<QuerySnapshot>(
            stream: db
                .collection('usersProfile')
                .doc(uid)
                .collection('address')
                .orderBy('primary', descending: true)
                .snapshots(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              } else {
                return (snapshot.data.docs.isNotEmpty)
                    ? ListView(
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        children: snapshot.data.docs.map((doc) {
                          return AddressList(
                            docId: doc.id,
                            name: (doc.data() as dynamic)['name'],
                            address: (doc.data() as dynamic)['address'],
                            primary: (doc.data() as dynamic)['primary'],
                          );
                        }).toList())
                    : const Center(child: EmptyAddres());
              }
            },
          ),
          const SizedBox(height: 15),
          ButtonSubmit(
            onPress: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const AddressFormScreen(),
                ),
              );
            },
            text: 'Add address',
          )
        ]),
      ),
    );
  }
}
