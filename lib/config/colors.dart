import 'package:flutter/material.dart';

const background = Color(0xFFf5f5f6);

const primaryColor = Color(0xFF1976d2);
const primaryDarkenColor = Color(0xFF004ba0);
const primaryLightenColor = Color(0xFF63a4ff);
