import 'package:cleanis/config/colors.dart';
import 'package:cleanis/profile/profile_screen.dart';
import 'package:cleanis/store/store_screen.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';

import 'basket/basket_screen.dart';
import 'home/home_screen.dart';

class App extends StatefulWidget {
  const App({Key key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  int _selectedTabIndex = 0;

  final List<Widget> _page = const <Widget>[
    HomeScreen(),
    StoreScreen(),
    BasketScreen(),
    ProfileScreen(),
  ];

  void _onBottomMenuTapped(int index) {
    setState(() {
      _selectedTabIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DoubleBackToCloseApp(
        snackBar: const SnackBar(
          content: Text('Tap back again to leave'),
        ),
        child: _page[_selectedTabIndex],
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white,
        type: BottomNavigationBarType.fixed,
        items: const [
          BottomNavigationBarItem(
            activeIcon: Icon(Icons.home_rounded),
            icon: Icon(Icons.home_outlined),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            activeIcon: Icon(Icons.local_laundry_service_rounded),
            icon: Icon(Icons.local_laundry_service),
            label: 'Store',
          ),
          BottomNavigationBarItem(
            activeIcon: Icon(Icons.receipt_long_rounded),
            icon: Icon(Icons.receipt_long_outlined),
            label: 'Transaction',
          ),
          BottomNavigationBarItem(
            activeIcon: Icon(Icons.face_rounded),
            icon: Icon(Icons.face),
            label: 'Profile',
          ),
        ],
        currentIndex: _selectedTabIndex,
        selectedItemColor: primaryColor,
        onTap: _onBottomMenuTapped,
      ),
    );
  }
}
