import 'dart:math';
import 'package:cleanis/config/styles.dart';
import 'package:cleanis/store/widget/search_input_widget.dart';
import 'package:cleanis/store/widget/store_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:cleanis/config/colors.dart';
import 'package:geolocator/geolocator.dart';

class StoreScreen extends StatefulWidget {
  const StoreScreen({key}) : super(key: key);

  @override
  _StoreScreenState createState() => _StoreScreenState();
}

class _StoreScreenState extends State<StoreScreen> {
  String inMode;
  String searchKey;
  void _searchOutlet(value) async {
    if (value == '') {
      setState(() {
        // searchKey = value;
        inMode = 'closest';
        searchKey = '';
      });
    } else {
      setState(() {
        inMode = 'search';
        searchKey = value;
      });
    }
    // Position position = await Geolocator.getCurrentPosition(
    //     desiredAccuracy: LocationAccuracy.best);
    // var storeDistance = [];
    // final data = await FirebaseFirestore.instance.collection('stores').get();

    // for (var element in data.docs) {
    //   String id = element.id;
    //   String name = element.get('name');
    //   String address = element.get('address');
    //   String thumbnail = element.get('thumbnail');
    //   String rate = element.get('rate');
    //   double lat = double.parse(element.get('lat'));
    //   double lng = double.parse(element.get('lng'));
    //   double distance =
    //       calculateDistance(position.latitude, position.longitude, lat, lng);

    //   storeDistance.add({
    //     "id": id,
    //     "name": name,
    //     "address": address,
    //     "thumbnail": thumbnail,
    //     "rate": rate,
    //     "distance": distance
    //   });
    //   for (var element in storeDistance) {
    //     element;
    //   }
    // }
    // storeDistance.clear();

    // print(storeDistance);
    // print(inMode);
    // return storeDistance;
  }

  Future<List> _loadData({mode, searchKey}) async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.best);
    var storeDistance = [];
    final data = await FirebaseFirestore.instance.collection('stores').get();

    for (var element in data.docs) {
      String id = element.id;
      String name = element.get('name');
      String address = element.get('address');
      String thumbnail = element.get('thumbnail');
      String rate = element.get('rate');
      double lat = double.parse(element.get('lat'));
      double lng = double.parse(element.get('lng'));
      double distance =
          calculateDistance(position.latitude, position.longitude, lat, lng);

      storeDistance.add({
        "id": id,
        "name": name,
        "address": address,
        "thumbnail": thumbnail,
        "rate": rate,
        "distance": distance
      });
      for (var element in storeDistance) {
        element;
      }
      if (mode == 'closest') {
        storeDistance.sort((a, b) => a["distance"].compareTo(b["distance"]));
      } else if (mode == 'rate') {
        storeDistance.sort((a, b) => b["rate"].compareTo(a["rate"]));
      } else if (mode == 'search') {
        storeDistance.retainWhere((element) {
          return element['name']
              .toLowerCase()
              .contains(searchKey.toLowerCase());
        });
      }
    }
    return storeDistance;
  }

  @override
  void initState() {
    super.initState();
    // inMode = 'closest';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: background,
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
        title: const Text(
          'Store',
          style: appBarStyleDark,
        ),
        elevation: 0.8,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            // const SearchInput(hint: 'Search your favorite doby...'),
            Container(
              height: 55,
              padding: const EdgeInsets.fromLTRB(15, 8, 15, 4),
              child: TextFormField(
                onChanged: (value) => _searchOutlet(value),
                style: const TextStyle(
                  fontSize: 16,
                ),
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.only(top: 10.0),
                  isDense: true,
                  filled: true,
                  fillColor: Colors.white,
                  prefixIcon: const Padding(
                    padding: EdgeInsets.only(left: 5.0, top: 4.0),
                    child: Icon(
                      Icons.search,
                      size: 27,
                      color: primaryColor,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    borderSide: const BorderSide(color: primaryColor),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    borderSide: const BorderSide(color: primaryColor),
                  ),
                  hintText: 'Search your favorite doby...',
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 15,
                vertical: 10,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  InputChip(
                    avatar: Icon(
                      Icons.place_rounded,
                      color:
                          (inMode == 'closest') ? Colors.white : Colors.black,
                    ),
                    backgroundColor:
                        (inMode == 'closest') ? primaryColor : Colors.grey[300],
                    label: Text(
                      'Lokasi terdekat',
                      style: TextStyle(
                        color:
                            (inMode == 'closest') ? Colors.white : Colors.black,
                      ),
                    ),
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    onPressed: () {
                      setState(() {
                        inMode = 'closest';
                      });
                    },
                  ),
                  const SizedBox(width: 10),
                  InputChip(
                    avatar: Icon(
                      Icons.star_rounded,
                      color: (inMode == 'rate') ? Colors.white : Colors.black,
                    ),
                    backgroundColor:
                        (inMode == 'rate') ? primaryColor : Colors.grey[300],
                    label: Text(
                      'Rating tertinggi',
                      style: TextStyle(
                        color: (inMode == 'rate') ? Colors.white : Colors.black,
                      ),
                    ),
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    onPressed: () {
                      setState(() {
                        inMode = 'rate';
                      });
                    },
                  ),
                ],
              ),
            ),
            const Divider(
              height: 2.0,
            ),
            Column(
              children: [
                FutureBuilder(
                    future: _loadData(mode: inMode, searchKey: searchKey),
                    builder: (BuildContext ctx, AsyncSnapshot<List> snapshot) {
                      if (snapshot.connectionState == ConnectionState.done) {
                        if (snapshot.hasData) {
                          return ListView.builder(
                            physics: const NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: snapshot.data.length,
                            itemBuilder: (BuildContext context, index) {
                              return Store(
                                firestoreDocID: snapshot.data[index]['id'],
                                title: snapshot.data[index]['name'],
                                addres: snapshot.data[index]['address'],
                                star: snapshot.data[index]['rate'],
                                image: snapshot.data[index]['thumbnail'],
                                distance: snapshot.data[index]['distance']
                                    .toStringAsFixed(0),
                              );
                            },
                          );
                        }
                      }
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    })
              ],
            )
          ],
        ),
      ),
    );
  }

  double calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 1000 * 12742 * asin(sqrt(a));
  }
}
