import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';

class OrderModel extends ChangeNotifier {
  final db = FirebaseFirestore.instance;
  final uid = FirebaseAuth.instance.currentUser.uid;

  // Order data
  final Map<String, dynamic> _orderService = {};
  Map get orderService => _orderService;

  // Address data
  final List _address = [];
  List get address => _address;

  // Operation Time Today
  final Map<String, dynamic> _operationTime = {};
  Map get operationTime => _operationTime;

  // Store to transactions
  Future storeTransaction() {
    const _chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    Random _rnd = Random();

    String getRandomString(int length) =>
        String.fromCharCodes(Iterable.generate(
            length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

    return db.collection('transactions').add({
      'created_at': DateTime.now(),
      'updated_at': DateTime.now(),
      'transactionId': getRandomString(8),
      'storeId': orderService['storeId'],
      'userId': uid,
      'orderServiceName': orderService['name'],
      'orderType': orderService['unit'],
      'dateOrder': orderService['dateOrder'],
      'pickupDate': orderService['pickupDate'] ?? '-',
      'pickupTime': orderService['pickupTime'] ?? '-',
      'deliveryDate': orderService['deliveryDate'] ?? '-',
      'deliveryTime': orderService['deliveryTime'] ?? '-',
      'totalOrder': orderService['total'],
      'price': orderService['prices'],
      'addressName': orderService['addressName'],
      'address': orderService['address'],
      'lat': orderService['lat'],
      'lng': orderService['lng'],
      'distance': orderService['distance'],
      'isComplain': false,
      'customerComplain': '',
      'storeComplain': '',
      'costDelivery': orderService['costDelivery'],
      'paymentMethod': orderService['paymentMethod'],
      'note': '',
      'rejected': false,
      'confirmed': false,
      'confirmStatusDate': '',
      'confirmStatusTime': '',
      'picked': false,
      'pickStatusDate': '',
      'pickStatusTime': '',
      'process': false,
      'processStatusDate': '',
      'processStatusTime': '',
      'shipped': false,
      'shippStatusDate': '',
      'shippStatusTime': '',
      'delivery': false,
      'deliverStatusDate': '',
      'deliverStatusTime': '',
      'isReviewed': false,
      'reviewedID': '',
    }).then((value) {
      return 'success';
    }).catchError((error) {
      return 'Error: $error';
    });
  }

  // OperationTime Data
  operationStoreTime({idStore}) {
    var date = DateTime.now();
    var idDoc = date.weekday.toString();
    db
        .collection('stores')
        .doc(idStore)
        .collection('operationTime')
        .doc(idDoc)
        .get()
        .then((DocumentSnapshot documentSnapshot) {
      if (documentSnapshot.exists) {
        operationTime.addAll(documentSnapshot.data());
        notifyListeners();
      }
    });
  }

  addDateDelivery({
    String pickedDate,
    String pickedTime,
    String deliveryDate,
    String deliveryTime,
  }) {
    if (double.parse(pickedTime) >= double.parse(operationTime['openHours']) &&
        double.parse(pickedTime) <= double.parse(operationTime['closeHours'])) {
      orderService['pickupDate'] = pickedDate;
      orderService['pickupTime'] = pickedTime;
      orderService['deliveryDate'] = deliveryDate;
      orderService['deliveryTime'] = deliveryTime;
      notifyListeners();
      return 'success';
    } else {
      return 'failed';
    }
  }

  // Location User (address)
  loadAddress({idUser, idStore}) async {
    await db
        .collection('usersProfile')
        .doc(idUser)
        .collection('address')
        .get()
        .then((QuerySnapshot querySnapshot) {
      if (address.isEmpty) {
        address.addAll(querySnapshot.docs);
      }
      if (address.isNotEmpty && orderService['addressName'] == null) {
        final item = address.firstWhere((e) => e['primary'] == true);
        if (item != null) {
          double distance = calculateDistance(
            double.parse(orderService['latStore']),
            double.parse(orderService['lngStore']),
            item['lat'],
            item['lng'],
          );
          addAddresUser(
            addressName: item['name'],
            address: item['address'],
            lat: item['lat'].toString(),
            lng: item['lng'].toString(),
            distance: distance.toString(),
          );
        }
      }
      notifyListeners();
    });
  }

  double calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 1000 * 12742 * asin(sqrt(a));
  }

  loadStore({String storeID}) {
    db
        .collection('stores')
        .doc(storeID)
        .get()
        .then((DocumentSnapshot documentSnapshot) {
      orderService['latStore'] = documentSnapshot.get('lat');
      orderService['lngStore'] = documentSnapshot.get('lng');
    });
  }

  addAddresUser(
      {String addressName,
      String address,
      String lat,
      String lng,
      String distance}) {
    // Hitung jarak

    orderService['addressName'] = addressName;
    orderService['address'] = address;
    orderService['lat'] = lat;
    orderService['lng'] = lng;
    orderService['distance'] = distance;
    notifyListeners();
  }

  loadService({idStore, idService}) {
    db
        .collection('stores')
        .doc(idStore)
        .collection('services')
        .doc(idService)
        .get()
        .then((DocumentSnapshot documentSnapshot) {
      if (documentSnapshot.exists) {
        if (orderService['total'] == null) {
          orderService['total'] = "1";
        }
        final currentDate = DateTime.now();
        orderService['dateOrder'] =
            DateFormat("EEEE, d MMMM yyyy hh:mm", "id_ID").format(currentDate);
        orderService['storeId'] = idStore;
        orderService['paymentMethod'] = 'COD';
        orderService.addAll(documentSnapshot.data());
        notifyListeners();
      }
    });
  }

  increamentItem({int currentTotal}) {
    var total = (currentTotal + 1).toString();
    orderService.update('total', (value) => total);
    notifyListeners();
  }

  decreamentItem({int currentTotal}) {
    var total = (currentTotal - 1).toString();
    orderService.update('total', (value) => total);
    notifyListeners();
  }
}
