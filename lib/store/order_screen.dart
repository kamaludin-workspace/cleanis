import 'package:cleanis/config/colors.dart';
import 'package:cleanis/config/styles.dart';
import 'package:cleanis/store/order_model.dart';
import 'package:flutter/material.dart';
import 'package:im_stepper/stepper.dart';
import 'package:provider/provider.dart';

import 'order_step/step_service.dart';
import 'order_step/step_location.dart';
import 'order_step/step_date.dart';
import 'order_step/step_confirm.dart';

class OrderScreen extends StatefulWidget {
  final String serviceID;
  final String storeID;
  const OrderScreen({Key key, this.serviceID, this.storeID}) : super(key: key);

  @override
  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  int activeStep = 0;
  int upperBound = 4;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: const Text(
          'Pesan layanan',
          style: appBarStyleLight,
        ),
      ),
      body: Column(
        children: [
          Container(
            color: Colors.white,
            child: IconStepper(
              activeStepColor: primaryColor,
              stepRadius: 22,
              activeStepBorderColor: primaryColor,
              activeStepBorderPadding: 2,
              enableNextPreviousButtons: false,
              lineDotRadius: 2,
              lineColor: Colors.grey,
              enableStepTapping: true,
              icons: const [
                Icon(Icons.local_laundry_service, color: Colors.white),
                Icon(Icons.location_on, color: Colors.white),
                Icon(Icons.date_range, color: Colors.white),
                Icon(Icons.check, color: Colors.white),
              ],
              activeStep: activeStep,
              onStepReached: (index) {
                setState(() {
                  activeStep = index;
                });
              },
            ),
          ),
          header(),
          Builder(builder: (context) {
            return ChangeNotifierProvider<OrderModel>(
              create: (BuildContext context) => OrderModel(),
              child: Expanded(
                child: contentStepper(),
              ),
            );
          }),
          (activeStep == 3)
              ? Container()
              : Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child:
                            (activeStep != 0) ? previousButton() : Container(),
                      ),
                      const SizedBox(width: 10),
                      Expanded(
                        child: nextButton(),
                      )
                    ],
                  ),
                )
        ],
      ),
    );
  }

  Widget nextButton() {
    return ElevatedButton(
      onPressed: () {
        if (activeStep < upperBound) {
          setState(() {
            activeStep++;
          });
        }
      },
      style: ElevatedButton.styleFrom(
        primary: primaryColor,
        padding: const EdgeInsets.symmetric(vertical: 12),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
      ),
      child: const Text(
        'SELANJUTNYA',
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
      ),
    );
  }

  Widget previousButton() {
    return ElevatedButton(
      onPressed: () {
        if (activeStep > 0) {
          setState(() {
            activeStep--;
          });
        }
      },
      style: ElevatedButton.styleFrom(
        primary: primaryColor,
        padding: const EdgeInsets.symmetric(vertical: 12),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
      ),
      child: const Text(
        'SEBELUMNYA',
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
      ),
    );
  }

  Widget header() {
    return Container(
      width: double.infinity,
      decoration: const BoxDecoration(
        border: Border(
          top: BorderSide(width: 0.2, color: Colors.grey),
        ),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(headerText(), style: headingTitle),
          Container(
            margin: const EdgeInsets.only(bottom: 5.0),
            height: 2,
            width: 40,
            color: primaryColor,
          )
        ],
      ),
    );
  }

  Widget contentStepper() {
    switch (activeStep) {
      case 0:
        return StepServices(
          serviceID: widget.serviceID,
          storeID: widget.storeID,
        );
      case 1:
        return StepLocation(
          storeID: widget.storeID,
        );
      case 2:
        return StepDates(
          storeID: widget.storeID,
        );
      case 3:
        return const StepConfirm();
      default:
        return const StepServices();
    }
  }

  String headerText() {
    switch (activeStep) {
      case 0:
        return 'Services';

      case 1:
        return 'Alamat pengiriman';

      case 2:
        return 'Tanggal';

      case 3:
        return 'Konfirmasi Pesanan';

      default:
        return 'Services';
    }
  }
}
