import 'package:cleanis/config/colors.dart';
import 'package:flutter/material.dart';

class SearchInput extends StatelessWidget {
  final String hint;
  final VoidCallback onChanged;
  const SearchInput({
    Key key,
    this.hint,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 55,
      padding: const EdgeInsets.fromLTRB(15, 8, 15, 4),
      child: TextFormField(
        style: const TextStyle(
          fontSize: 16,
        ),
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.only(top: 10.0),
          isDense: true,
          filled: true,
          fillColor: Colors.white,
          prefixIcon: const Padding(
            padding: EdgeInsets.only(left: 5.0, top: 4.0),
            child: Icon(
              Icons.search,
              size: 27,
              color: primaryColor,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8.0),
            borderSide: const BorderSide(color: primaryColor),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8.0),
            borderSide: const BorderSide(color: primaryColor),
          ),
          hintText: hint,
        ),
      ),
    );
  }
}
