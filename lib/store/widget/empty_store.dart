import 'package:flutter/material.dart';

class EmptyStore extends StatelessWidget {
  final String text;
  const EmptyStore({
    Key key,
    this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        const SizedBox(height: 120),
        Image.asset(
          'assets/images/laundry_store.png',
          scale: 3,
        ),
        const SizedBox(height: 20),
        Text(
          text,
          style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
        const Text(
          'Silakan ulangi beberapa saat lagi',
          style: TextStyle(fontSize: 17, fontWeight: FontWeight.normal),
        )
      ],
    );
  }
}
