import 'package:cleanis/config/colors.dart';
import 'package:flutter/material.dart';

class Location extends StatelessWidget {
  final String location;
  const Location({
    Key key,
    this.location,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.white,
      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 13),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(
          bottom: BorderSide(
            width: 1.8,
            color: Colors.grey[300],
          ),
        ),
      ),
      child: Row(
        children: <Widget>[
          const Icon(
            Icons.location_on,
            size: 16,
          ),
          Text(
            location,
            style: const TextStyle(
              color: Colors.black,
              fontSize: 16,
              fontWeight: FontWeight.w600,
            ),
          ),
          const Spacer(),
          const Icon(
            Icons.near_me,
            size: 15,
            color: primaryColor,
          ),
          const Text(
            ' CHANGE',
            style: TextStyle(
              color: primaryColor,
              fontSize: 12,
              fontWeight: FontWeight.w600,
            ),
          )
        ],
      ),
    );
  }
}
