import 'package:flutter/material.dart';

class TabTitle extends StatelessWidget {
  final String title;
  const TabTitle({
    Key key,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: const TextStyle(fontSize: 16.0, fontWeight: FontWeight.w600),
    );
  }
}
