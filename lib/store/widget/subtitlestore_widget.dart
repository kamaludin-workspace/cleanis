import 'package:flutter/material.dart';

class SubtitleStore extends StatelessWidget {
  final String title;
  const SubtitleStore({
    Key key,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: const TextStyle(
        fontSize: 20,
        fontWeight: FontWeight.w700,
      ),
    );
  }
}
