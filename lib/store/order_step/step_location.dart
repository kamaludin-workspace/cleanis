import 'dart:math';

import 'package:cleanis/config/colors.dart';
import 'package:cleanis/profile/address_form_screen.dart';
import 'package:cleanis/store/order_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class StepLocation extends StatefulWidget {
  final String storeID;
  const StepLocation({key, this.storeID}) : super(key: key);

  @override
  _StepLocationState createState() => _StepLocationState();
}

class _StepLocationState extends State<StepLocation> {
  final uid = FirebaseAuth.instance.currentUser.uid;
  final nominal = NumberFormat("#,##0", "en_US");

  @override
  void initState() {
    super.initState();
    getAddress();
  }

  void getAddress() async {
    await Provider.of<OrderModel>(context, listen: false)
        .loadAddress(idUser: uid);
  }

  double calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 1000 * 12742 * asin(sqrt(a));
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Consumer<OrderModel>(builder: (context, orderModel, child) {
        return (orderModel.orderService.isNotEmpty)
            ? Container(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: (orderModel.orderService['addressName'] != null)
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: Colors.blue[800],
                              border: Border.all(color: primaryColor),
                              borderRadius: const BorderRadius.all(
                                Radius.circular(8.0),
                              ),
                            ),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 15),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  orderModel.orderService['addressName'] ?? '',
                                  style: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  orderModel.orderService['address'] ?? '',
                                  style: const TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                  ),
                                ),
                                Text(
                                  'Rp. ' +
                                          (nominal.format((double.parse(
                                                      orderModel.orderService[
                                                          'distance']) *
                                                  0.001) *
                                              int.parse(orderModel.orderService[
                                                  'costDelivery']))) ??
                                      '',
                                  style: const TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                  ),
                                )
                              ],
                            ),
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: TextButton.icon(
                                  style: TextButton.styleFrom(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8),
                                      side: const BorderSide(
                                          color: primaryColor, width: 2),
                                    ),
                                    backgroundColor: Colors.white,
                                  ),
                                  onPressed: () {
                                    // Belum jumpa cara refresh
                                  },
                                  icon: const Icon(
                                    Icons.refresh_rounded,
                                    color: primaryColor,
                                  ),
                                  label: const Text(
                                    "Reload alamat",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                      color: primaryColor,
                                    ),
                                  ),
                                ),
                              ),
                              const SizedBox(width: 5),
                              Expanded(
                                child: TextButton.icon(
                                  style: TextButton.styleFrom(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    backgroundColor: primaryColor,
                                  ),
                                  onPressed: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) =>
                                            const AddressFormScreen(),
                                      ),
                                    );
                                  },
                                  icon: const Icon(
                                    Icons.place,
                                    color: Colors.white,
                                  ),
                                  label: const Text(
                                    "Alamat baru",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          (orderModel.address.length > 1)
                              ? Container(
                                  margin:
                                      const EdgeInsets.symmetric(vertical: 10),
                                  child: const Text(
                                    'Alamat',
                                    style: TextStyle(fontSize: 18),
                                  ),
                                )
                              : Container(height: 10),
                          (orderModel.address.length > 1)
                              ? ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: orderModel.address.length,
                                  itemBuilder: (context, index) {
                                    return Container(
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: const BorderRadius.all(
                                            Radius.circular(8.0)),
                                        border: Border.all(
                                            width: 1, color: Colors.grey),
                                      ),
                                      margin: const EdgeInsets.only(bottom: 5),
                                      padding: const EdgeInsets.all(15),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  '${orderModel.address[index]['name']}' +
                                                      ((orderModel.address[
                                                              index]['primary'])
                                                          ? ' (Utama)'
                                                          : ''),
                                                  style: const TextStyle(
                                                    fontSize: 20,
                                                    height: 1.3,
                                                    fontWeight: FontWeight.w600,
                                                  ),
                                                ),
                                                const SizedBox(height: 5),
                                                Text(
                                                  '${orderModel.address[index]['address']}',
                                                  style: TextStyle(
                                                    fontSize: 14,
                                                    color: Colors.grey[600],
                                                    fontWeight: FontWeight.w600,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                          (orderModel.orderService[
                                                      'addressName'] !=
                                                  orderModel.address[index]
                                                      ['name'])
                                              ? TextButton(
                                                  style: TextButton.styleFrom(
                                                    shape:
                                                        RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              8),
                                                    ),
                                                    backgroundColor:
                                                        primaryColor,
                                                  ),
                                                  onPressed: () async {
                                                    double distance =
                                                        calculateDistance(
                                                      double.parse(orderModel
                                                              .orderService[
                                                          'latStore']),
                                                      double.parse(orderModel
                                                              .orderService[
                                                          'lngStore']),
                                                      orderModel.address[index]
                                                          ['lat'],
                                                      orderModel.address[index]
                                                          ['lng'],
                                                    );
                                                    await Provider.of<OrderModel>(
                                                            context,
                                                            listen: false)
                                                        .addAddresUser(
                                                            addressName:
                                                                orderModel.address[index]
                                                                    ['name'],
                                                            address: orderModel
                                                                    .address[index]
                                                                ['address'],
                                                            lat: orderModel
                                                                .address[index]
                                                                    ['lat']
                                                                .toString(),
                                                            lng: orderModel
                                                                .address[index]
                                                                    ['lng']
                                                                .toString(),
                                                            distance:
                                                                distance.toString());
                                                  },
                                                  child: const Text(
                                                    "PILIH",
                                                    style: TextStyle(
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.white,
                                                    ),
                                                  ),
                                                )
                                              : Container()
                                        ],
                                      ),
                                    );
                                  },
                                )
                              : Container(),
                        ],
                      )
                    : Column(
                        children: [
                          const SizedBox(height: 40),
                          Image.asset(
                            'assets/images/address.png',
                            scale: 1,
                          ),
                          const SizedBox(height: 20),
                          TextButton.icon(
                            style: TextButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                              ),
                              backgroundColor: primaryColor,
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      const AddressFormScreen(),
                                ),
                              );
                            },
                            icon: const Icon(
                              Icons.place,
                              color: Colors.white,
                            ),
                            label: const Text(
                              "Alamat Baru  ",
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                              ),
                            ),
                          )
                        ],
                      ),
              )
            : Container();
      }),
    );
  }
}
