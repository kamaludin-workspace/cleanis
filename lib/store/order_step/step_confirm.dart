import 'package:cleanis/basket/basket_screen.dart';
import 'package:cleanis/config/colors.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../order_model.dart';

enum methodPayment { cod, tf }

class StepConfirm extends StatefulWidget {
  const StepConfirm({key}) : super(key: key);

  @override
  _StepConfirmState createState() => _StepConfirmState();
}

class _StepConfirmState extends State<StepConfirm> {
  methodPayment _payment = methodPayment.cod;

  final nominal = NumberFormat("#,##0", "en_US");
  bool isError = false;

  formatOngkir(price) {
    String text;
    if (price == '0') {
      return text = ' Gratis';
    } else {
      var proc = price;
      text = ' Rp. ' + proc;
      return text;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<OrderModel>(
      builder: (context, orderModel, child) {
        return Column(
          children: [
            Expanded(
              flex: 80,
              child: Container(
                height: MediaQuery.of(context).size.width * 0.90,
                width: double.infinity,
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      (isError)
                          ? Container(
                              width: double.infinity,
                              decoration: BoxDecoration(
                                color: Colors.red[300],
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(8.0)),
                                border: Border.all(
                                    width: 1, color: Colors.grey[300]),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.2),
                                    spreadRadius: 1,
                                    blurRadius: 4,
                                    offset: const Offset(0, 3),
                                  ),
                                ],
                              ),
                              padding: const EdgeInsets.all(10),
                              margin: const EdgeInsets.only(bottom: 10),
                              child: const Text(
                                'Kesalahan tidak terduga, ulangi proses dari awal',
                                style: TextStyle(color: Colors.white),
                              ),
                            )
                          : Container(),
                      Text(
                        orderModel.orderService['name'] ?? '',
                        style: const TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          color: primaryDarkenColor,
                        ),
                      ),
                      const SizedBox(height: 5),
                      ServicesDetailText(
                        title: 'Tanggal order',
                        subtile: orderModel.orderService['dateOrder'] ?? '',
                      ),
                      ServicesDetailText(
                        title: 'Pickup',
                        subtile: (orderModel.orderService['pickupDate'] != null)
                            ? '${orderModel.orderService['pickupDate']}, ${orderModel.orderService['pickupTime']}'
                            : 'Kosong',
                      ),
                      ServicesDetailText(
                        title: 'Delivery',
                        subtile: (orderModel.orderService['deliveryDate'] !=
                                null)
                            ? '${orderModel.orderService['deliveryDate']}, ${orderModel.orderService['deliveryTime']}'
                            : 'Kosong',
                      ),
                      ServicesDetailText(
                        title: 'Alamat',
                        subtile: orderModel.orderService['address'] ?? '',
                      ),
                      const Divider(thickness: 2, height: 20),
                      (orderModel.orderService['unit'] == 'Kg')
                          ? const Text('Berat kg akan diupdate toko',
                              style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.bold,
                              ))
                          : Container(),
                      ServicesDetailText(
                        title:
                            'Rp. ${nominal.format(int.parse(orderModel.orderService['prices']))} X ${orderModel.orderService['total']} ${orderModel.orderService['unit']}',
                        subtile: 'Rp. ' +
                                nominal
                                    .format((int.parse(
                                            orderModel.orderService['total']) *
                                        int.parse(
                                            orderModel.orderService['prices'])))
                                    .toString() ??
                            '',
                      ),
                      ServicesDetailText(
                        title:
                            'Rp. ${nominal.format((int.parse(orderModel.orderService['costDelivery'])))} /Km X ${(double.parse(orderModel.orderService['distance'])).toStringAsFixed(0)} M',
                        subtile: formatOngkir((nominal.format(
                            (double.parse(orderModel.orderService['distance']) *
                                    0.001) *
                                double.parse(
                                    orderModel.orderService['costDelivery'])))),
                      ),
                      const SizedBox(height: 15),
                      const Text(
                        'Metode Pembayaran',
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w600),
                      ),
                      Container(
                        padding: const EdgeInsets.fromLTRB(0, 10, 0, 20),
                        child: Column(
                          children: [
                            Container(
                              width: double.infinity,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(color: primaryColor),
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(8.0),
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.2),
                                    spreadRadius: 1,
                                    blurRadius: 4,
                                    offset: const Offset(0, 3),
                                  ),
                                ],
                              ),
                              child: RadioListTile<methodPayment>(
                                value: methodPayment.cod,
                                groupValue: _payment,
                                onChanged: (value) {
                                  setState(() {
                                    _payment = value;
                                  });
                                },
                                title: const Text(
                                  "Cash on Delivery",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600),
                                ),
                                selected: true,
                                toggleable: true,
                                controlAffinity:
                                    ListTileControlAffinity.trailing,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 25,
              child: Container(
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border(
                      top: BorderSide(width: 1, color: Colors.grey[300])),
                ),
                padding: const EdgeInsets.symmetric(vertical: 15),
                child: Container(
                  decoration: BoxDecoration(
                    color: (isError) ? Colors.grey : primaryColor,
                    border: Border.all(color: primaryColor),
                    borderRadius: const BorderRadius.all(
                      Radius.circular(8.0),
                    ),
                  ),
                  margin: const EdgeInsets.symmetric(horizontal: 10),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                  child: Row(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Pembayaran',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                                fontWeight: FontWeight.w500),
                          ),
                          Text(
                            'Rp. ' +
                                    nominal
                                        .format((int.parse(orderModel
                                                    .orderService['total']) *
                                                int.parse(orderModel
                                                    .orderService['prices'])) +
                                            (double.parse(
                                                        orderModel.orderService[
                                                            'distance']) *
                                                    0.001) *
                                                int.parse(
                                                    orderModel.orderService[
                                                        'costDelivery']))
                                        .toString() ??
                                '',
                            style: const TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.w600),
                          ),
                        ],
                      ),
                      const Spacer(),
                      TextButton(
                        onPressed: () async {
                          if (!isError) {
                            var result = await orderModel.storeTransaction();
                            if (result == 'success') {
                              Navigator.of(context).pushReplacement(
                                MaterialPageRoute(
                                    builder: (_) => const BasketScreen()),
                              );
                            } else {
                              setState(() {
                                isError = true;
                              });
                            }
                          }
                        },
                        style: TextButton.styleFrom(
                          padding: const EdgeInsets.symmetric(vertical: 4),
                        ),
                        child: Container(
                          padding: const EdgeInsets.symmetric(vertical: 2),
                          child: Row(
                            children: const [
                              Text(
                                'Pesan Sekarang',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w600),
                              ),
                              SizedBox(width: 10),
                              FaIcon(
                                FontAwesomeIcons.arrowCircleRight,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        );
      },
    );
  }
}

class ServicesDetailText extends StatelessWidget {
  final String title, subtile;

  const ServicesDetailText({
    Key key,
    this.title,
    this.subtile,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.symmetric(vertical: 2),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: const TextStyle(color: Colors.black87),
          ),
          Flexible(
            child: Container(
              width: double.infinity,
              alignment: Alignment.topRight,
              margin: const EdgeInsets.only(left: 60.0),
              child: Text(
                subtile,
                overflow: TextOverflow.clip,
                textAlign: TextAlign.right,
                style: const TextStyle(fontWeight: FontWeight.w600),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
