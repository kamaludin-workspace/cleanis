import 'package:cleanis/config/colors.dart';
import 'package:cleanis/config/styles.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:cleanis/store/order_model.dart';

class StepServices extends StatefulWidget {
  final String serviceID;
  final String storeID;
  const StepServices({key, this.serviceID, this.storeID}) : super(key: key);

  @override
  _StepServicesState createState() => _StepServicesState();
}

class _StepServicesState extends State<StepServices> {
  final nominal = NumberFormat("#,##0", "en_US");

  @override
  void initState() {
    super.initState();
    Provider.of<OrderModel>(context, listen: false)
        .loadStore(storeID: widget.storeID);
    loadModelFromService();
  }

  void loadModelFromService() async {
    await Provider.of<OrderModel>(context, listen: false).loadService(
      idStore: widget.storeID,
      idService: widget.serviceID,
    );
  }

  formatOngkir(price) {
    String text;
    if (price == '0') {
      return text = ' Gratis';
    } else {
      var proc = nominal.format(int.parse(price));
      text = ' Rp. ' + proc + '/Km';
      return text;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<OrderModel>(builder: (context, orderModel, child) {
      return (orderModel.orderService.isNotEmpty)
          ? Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 3,
                        child: Container(
                          width: double.infinity,
                          height: 90,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage(
                                  "assets/images/services/${orderModel.orderService['thumbnail']}"),
                              scale: 8,
                            ),
                            border: Border.all(
                              color: Colors.grey,
                            ),
                            borderRadius: const BorderRadius.all(
                              Radius.circular(5),
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 7,
                        child: Container(
                          padding: const EdgeInsets.only(left: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                orderModel.orderService['name'],
                                style: const TextStyle(
                                  fontSize: 24,
                                  color: Colors.black87,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Row(
                                children: [
                                  const Icon(
                                    Icons.schedule,
                                    size: 20,
                                    color: primaryColor,
                                  ),
                                  Text(
                                    ' ' +
                                        orderModel.orderService['processTime'],
                                    style: const TextStyle(
                                      color: primaryDarkenColor,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w100,
                                    ),
                                  ),
                                  const SizedBox(width: 5),
                                  const Icon(
                                    Icons.delivery_dining,
                                    size: 24,
                                    color: primaryColor,
                                  ),
                                  Text(
                                    formatOngkir(orderModel
                                        .orderService['costDelivery']),
                                    style: const TextStyle(
                                      color: primaryDarkenColor,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w100,
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(height: 15),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.baseline,
                                    textBaseline: TextBaseline.alphabetic,
                                    children: [
                                      const Text('Rp. ',
                                          style: TextStyle(fontSize: 16)),
                                      Text(
                                          nominal
                                              .format(int.parse(orderModel
                                                      .orderService['prices']) *
                                                  int.parse(orderModel
                                                      .orderService['total']))
                                              .toString(),
                                          style: const TextStyle(fontSize: 20)),
                                      Text(
                                        '/' + orderModel.orderService['unit'],
                                        style: const TextStyle(fontSize: 16),
                                      ),
                                    ],
                                  ),
                                  const Spacer(),
                                  (orderModel.orderService['unit'] == 'Pcs')
                                      ? Row(
                                          children: [
                                            ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              child: Material(
                                                color: int.parse(orderModel
                                                                .orderService[
                                                            'total']) >
                                                        1
                                                    ? primaryColor
                                                    : Colors.grey,
                                                child: InkWell(
                                                  splashColor: Colors.grey,
                                                  child: SizedBox(
                                                      width: 26,
                                                      height: 26,
                                                      child: Icon(
                                                        Icons.remove,
                                                        size: 17,
                                                        color: int.parse(orderModel
                                                                        .orderService[
                                                                    'total']) >
                                                                1
                                                            ? Colors.white
                                                            : Colors.black,
                                                      )),
                                                  onTap: () {
                                                    if (int.parse(orderModel
                                                                .orderService[
                                                            'total']) >
                                                        1) {
                                                      Provider.of<OrderModel>(
                                                              context,
                                                              listen: false)
                                                          .decreamentItem(
                                                              currentTotal: int
                                                                  .parse(orderModel
                                                                          .orderService[
                                                                      'total']));
                                                    }
                                                  },
                                                ),
                                              ),
                                            ),
                                            Container(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 15),
                                              child: Text(
                                                  "${int.parse(orderModel.orderService['total'])}",
                                                  style: const TextStyle(
                                                    fontSize: 20,
                                                    color: Colors.black,
                                                    fontWeight: FontWeight.w700,
                                                  )),
                                            ),
                                            ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                              child: Material(
                                                color: int.parse(orderModel
                                                                .orderService[
                                                            'total']) <
                                                        10
                                                    ? primaryColor
                                                    : Colors.grey,
                                                child: InkWell(
                                                  splashColor: Colors.grey,
                                                  child: SizedBox(
                                                      width: 26,
                                                      height: 26,
                                                      child: Icon(
                                                        Icons.add,
                                                        size: 17,
                                                        color: int.parse(orderModel
                                                                        .orderService[
                                                                    'total']) <
                                                                10
                                                            ? Colors.white
                                                            : Colors.black,
                                                      )),
                                                  onTap: () {
                                                    if (int.parse(orderModel
                                                                .orderService[
                                                            'total']) <
                                                        10) {
                                                      Provider.of<OrderModel>(
                                                              context,
                                                              listen: false)
                                                          .increamentItem(
                                                              currentTotal: int
                                                                  .parse(orderModel
                                                                          .orderService[
                                                                      'total']));
                                                    }
                                                  },
                                                ),
                                              ),
                                            )
                                          ],
                                        )
                                      : Container(),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 15),
                  const Text('Deskripsi', style: titleStyle),
                  const SizedBox(height: 10),
                  Text(
                    orderModel.orderService['description'],
                    style: const TextStyle(
                      height: 1.2,
                      fontSize: 15,
                      letterSpacing: 0.4,
                      color: Colors.black54,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            )
          : const Center(
              child: CircularProgressIndicator(),
            );
    });
  }
}
