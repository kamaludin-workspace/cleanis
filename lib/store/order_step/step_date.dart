import 'package:cleanis/config/colors.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../order_model.dart';

class StepDates extends StatefulWidget {
  final String storeID;
  const StepDates({key, this.storeID}) : super(key: key);

  @override
  _StepDatesState createState() => _StepDatesState();
}

class _StepDatesState extends State<StepDates> {
  final _formKey = GlobalKey<FormState>();
  bool isError = false;
  bool isSuccess = false;

  // Initial date and time
  DateTime _date = DateTime.now();
  TimeOfDay _time = TimeOfDay.now();

  // controller
  final pickedDateController = TextEditingController();
  final pickedTimeController = TextEditingController();
  final deliveryDateController = TextEditingController();
  final deliveryTimeController = TextEditingController();

  // Method for picked
  Future<void> _selectPickedDate(BuildContext context) async {
    bool _limitDay(DateTime day) {
      if ((day.isAfter(DateTime.now().subtract(const Duration(days: 1))) &&
          day.isBefore(DateTime.now().add(const Duration(days: 10))))) {
        return true;
      }
      return false;
    }

    DateTime _datePicked = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(DateTime.now().year),
      lastDate: DateTime(DateTime.now().year + 1),
      selectableDayPredicate: _limitDay,
    );

    if (_datePicked != null) {
      _date = _datePicked;
      pickedDateController.text =
          DateFormat("EEEE, d MMMM yyyy", "id_ID").format(_date);
    }
  }

  Future<void> _selectPickedTime(BuildContext context) async {
    final TimeOfDay _pickedTime = await showTimePicker(
      context: context,
      initialTime: _time,
      builder: (BuildContext context, Widget child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
          child: child,
        );
      },
    );

    if (_pickedTime != null) {
      _time = _pickedTime;
      var hour = (_time.hour < 9) ? ('0' + _time.hour.toString()) : _time.hour;
      var minute =
          (_time.minute < 9) ? ('0' + _time.minute.toString()) : _time.minute;
      pickedTimeController.text = '$hour.$minute';
    }
  }

  // Method for delivery
  Future<void> _selectDeliveryDate(BuildContext context) async {
    bool _limitDay(DateTime day) {
      if ((day.isAfter(DateTime.now().subtract(const Duration(days: 1))) &&
          day.isBefore(DateTime.now().add(const Duration(days: 10))))) {
        return true;
      }
      return false;
    }

    DateTime _datePicked = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(DateTime.now().year),
      lastDate: DateTime(DateTime.now().year + 1),
      selectableDayPredicate: _limitDay,
    );

    if (_datePicked != null) {
      _date = _datePicked;
      deliveryDateController.text =
          DateFormat("EEEE, d MMMM yyyy", "id_ID").format(_date);
    }
  }

  Future<void> _selectDeliveryTime(BuildContext context) async {
    final TimeOfDay _pickedTime = await showTimePicker(
      context: context,
      initialTime: _time,
      builder: (BuildContext context, Widget child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
          child: child,
        );
      },
    );

    if (_pickedTime != null) {
      _time = _pickedTime;
      var hour = (_time.hour < 9) ? ('0' + _time.hour.toString()) : _time.hour;
      var minute =
          (_time.minute < 9) ? ('0' + _time.minute.toString()) : _time.minute;
      deliveryTimeController.text = '$hour.$minute';
    }
  }

  @override
  void initState() {
    super.initState();
    Provider.of<OrderModel>(context, listen: false).operationStoreTime(
      idStore: widget.storeID,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            (isError)
                ? Container(
                    decoration: BoxDecoration(
                      color: Colors.red[300],
                      borderRadius:
                          const BorderRadius.all(Radius.circular(8.0)),
                      border: Border.all(width: 1, color: Colors.grey[300]),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          spreadRadius: 1,
                          blurRadius: 4,
                          offset: const Offset(0, 3),
                        ),
                      ],
                    ),
                    padding: const EdgeInsets.all(10),
                    margin: const EdgeInsets.only(bottom: 10),
                    child: const Text(
                      'Pastikan tanggal anda sesuai dengan waktu/tanggal toko',
                      style: TextStyle(color: Colors.white),
                    ),
                  )
                : Container(),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: const BorderRadius.all(Radius.circular(8.0)),
                border: Border.all(width: 1, color: Colors.grey[300]),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.2),
                    spreadRadius: 1,
                    blurRadius: 4,
                    offset: const Offset(0, 3),
                  ),
                ],
              ),
              padding: const EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'Tanggal/waktu penjemputan:',
                    style: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  const SizedBox(height: 15),
                  Row(
                    children: [
                      Expanded(
                        flex: 2,
                        child: TextFormField(
                          controller: pickedDateController,
                          readOnly: true,
                          onTap: () async {
                            _selectPickedDate(context);
                          },
                          validator: (val) =>
                              val.isEmpty ? 'Tidak boleh kosong' : null,
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            isDense: true,
                            contentPadding: EdgeInsets.only(left: 10),
                            suffixIcon: Icon(
                              Icons.calendar_today,
                              size: 18,
                            ),
                            labelText: 'Tanggal',
                          ),
                        ),
                      ),
                      const SizedBox(width: 5),
                      Expanded(
                        flex: 1,
                        child: TextFormField(
                          controller: pickedTimeController,
                          readOnly: true,
                          onTap: () async {
                            _selectPickedTime(context);
                          },
                          validator: (val) =>
                              val.isEmpty ? 'Tidak boleh kosong' : null,
                          decoration: const InputDecoration(
                            isDense: true,
                            contentPadding: EdgeInsets.only(left: 10),
                            border: OutlineInputBorder(),
                            suffixIcon: Icon(
                              Icons.schedule,
                              size: 18,
                            ),
                            labelText: 'Jam',
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 5),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: const BorderRadius.all(Radius.circular(8.0)),
                border: Border.all(width: 1, color: Colors.grey[300]),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.2),
                    spreadRadius: 1,
                    blurRadius: 4,
                    offset: const Offset(0, 3),
                  ),
                ],
              ),
              padding: const EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'Tanggal/waktu antar:',
                    style: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  const SizedBox(height: 15),
                  Row(
                    children: [
                      Expanded(
                        flex: 2,
                        child: TextFormField(
                          controller: deliveryDateController,
                          readOnly: true,
                          onTap: () async {
                            _selectDeliveryDate(context);
                          },
                          validator: (val) =>
                              val.isEmpty ? 'Tidak boleh kosong' : null,
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            isDense: true,
                            contentPadding: EdgeInsets.only(left: 10),
                            suffixIcon: Icon(
                              Icons.calendar_today,
                              size: 18,
                            ),
                            labelText: 'Tanggal',
                          ),
                        ),
                      ),
                      const SizedBox(width: 5),
                      Expanded(
                        flex: 1,
                        child: TextFormField(
                          controller: deliveryTimeController,
                          readOnly: true,
                          onTap: () async {
                            _selectDeliveryTime(context);
                          },
                          validator: (val) =>
                              val.isEmpty ? 'Tidak boleh kosong' : null,
                          decoration: const InputDecoration(
                            isDense: true,
                            contentPadding: EdgeInsets.only(left: 10),
                            border: OutlineInputBorder(),
                            suffixIcon: Icon(
                              Icons.schedule,
                              size: 18,
                            ),
                            labelText: 'Jam',
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(height: 10),
            Row(
              children: [
                Expanded(
                  child: TextButton(
                    style: TextButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                        side: const BorderSide(color: primaryColor, width: 2),
                      ),
                      backgroundColor: Colors.white,
                    ),
                    onPressed: () {
                      setState(() {
                        isError = false;
                        isSuccess = false;
                      });
                      pickedDateController.clear();
                      pickedTimeController.clear();
                      deliveryDateController.clear();
                      deliveryTimeController.clear();
                    },
                    child: const Text(
                      "Reset",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: primaryColor,
                      ),
                    ),
                  ),
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: TextButton(
                    style: TextButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      backgroundColor:
                          (isSuccess) ? Colors.green[300] : primaryColor,
                    ),
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        final sendDate =
                            Provider.of<OrderModel>(context, listen: false)
                                .addDateDelivery(
                          pickedDate: pickedDateController.text,
                          pickedTime: pickedTimeController.text,
                          deliveryDate: deliveryDateController.text,
                          deliveryTime: deliveryTimeController.text,
                        );
                        if (sendDate == 'success') {
                          setState(() {
                            isError = false;
                            isSuccess = true;
                          });
                        } else {
                          setState(() {
                            isSuccess = false;
                            isError = true;
                          });
                        }
                      }
                    },
                    child: Text(
                      (isSuccess) ? "Jadwal sesuai" : "Submit",
                      style: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
