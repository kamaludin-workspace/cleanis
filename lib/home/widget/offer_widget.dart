import 'package:cleanis/config/colors.dart';
import 'package:flutter/material.dart';

class OffersCard extends StatelessWidget {
  final String title, subtitle, voucher, date;
  const OffersCard({
    Key key,
    this.title,
    this.subtitle,
    this.voucher,
    this.date,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 280,
      margin: const EdgeInsets.only(right: 20),
      height: 160,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: const BorderRadius.all(
          Radius.circular(8.0),
        ),
        border: Border.all(
          width: 1,
          color: Colors.grey[300],
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            height: 95,
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.only(
                topRight: Radius.circular(8),
                topLeft: Radius.circular(8),
              ),
              image: DecorationImage(
                image: AssetImage('assets/images/$voucher'),
                fit: BoxFit.cover,
              ),
            ),
            child: Container(
              width: double.infinity,
              padding: const EdgeInsets.fromLTRB(10, 30, 90, 20),
              child: Text(
                title,
                style: const TextStyle(
                  fontFamily: 'NunitoBold',
                  color: Colors.black,
                  height: 1.1,
                  fontSize: 22,
                  fontWeight: FontWeight.w900,
                ),
              ),
            ),
          ),
          Container(
            height: 55,
            decoration: BoxDecoration(
              border: Border(
                top: BorderSide(
                  width: 0.8,
                  color: Colors.grey[300],
                ),
              ),
            ),
            padding: const EdgeInsets.fromLTRB(10, 5, 10, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  flex: 3,
                  child: Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      subtitle,
                      style: const TextStyle(
                        fontSize: 15,
                        height: 1.1,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                ),
                Row(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(bottom: 2),
                      child: const Icon(
                        Icons.calendar_today,
                        size: 11,
                        color: primaryColor,
                      ),
                    ),
                    const SizedBox(width: 5),
                    Text(
                      'Berlaku hingga $date',
                      style: const TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
