import 'package:flutter/material.dart';

class TopService extends StatelessWidget {
  const TopService({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: const <Widget>[
          ServiceCard(
            image: 'assets/images/topservices/wash.jpg',
            title: 'Wash & Fold',
          ),
          ServiceCard(
            image: 'assets/images/topservices/iron.jpg',
            title: 'Wash & Iron',
          ),
          ServiceCard(
            image: 'assets/images/topservices/dry.jpg',
            title: 'Dry Clean',
          ),
          ServiceCard(
            image: 'assets/images/topservices/premium.jpg',
            title: 'Premium Serv',
          ),
        ],
      ),
    );
  }
}

class ServiceCard extends StatelessWidget {
  final String title;
  final String image;

  const ServiceCard({
    Key key,
    this.title,
    this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 4),
      child: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(
                  blurRadius: 10,
                  color: Colors.grey[300],
                  spreadRadius: 1,
                  offset: const Offset(0, 3),
                )
              ],
            ),
            child: CircleAvatar(
              radius: 30,
              backgroundColor: Colors.white,
              backgroundImage: AssetImage(image),
            ),
          ),
          const SizedBox(height: 5),
          Text(
            title,
            style: const TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w500,
            ),
          )
        ],
      ),
    );
  }
}
