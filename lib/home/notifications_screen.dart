import 'package:cleanis/config/colors.dart';
import 'package:cleanis/config/styles.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class NotificationsScreen extends StatefulWidget {
  final String uid;
  const NotificationsScreen({key, this.uid}) : super(key: key);

  @override
  _NotificationsScreenState createState() => _NotificationsScreenState();
}

class _NotificationsScreenState extends State<NotificationsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: const IconThemeData(
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
        title: const Center(
          child: Text(
            'Notifications',
            style: appBarStyleDark,
          ),
        ),
        elevation: 3,
      ),
      body: Column(
        children: [
          FutureBuilder(
            future: FirebaseFirestore.instance
                .collection('usersProfile')
                .doc(widget.uid)
                .collection('notifications')
                .where('isArchive', isEqualTo: false)
                .get(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasData) {
                  try {
                    if (snapshot.data.docs.length > 0) {
                      return MediaQuery.removePadding(
                        context: context,
                        removeTop: true,
                        child: ListView.builder(
                            physics: const NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: snapshot.data.docs.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Slidable(
                                actionPane: const SlidableDrawerActionPane(),
                                actionExtentRatio: 0.25,
                                child: Container(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 10),
                                  color: (snapshot.data.docs[index]['isRead'])
                                      ? Colors.white
                                      : Colors.grey[200],
                                  child: ListTile(
                                    leading: CircleAvatar(
                                      backgroundColor: primaryColor,
                                      child: Text((index + 1).toString()),
                                      foregroundColor: Colors.white,
                                    ),
                                    title: Text(
                                        snapshot.data.docs[index]['title']),
                                    subtitle: Text(snapshot.data.docs[index]
                                        ['description']),
                                  ),
                                ),
                                actions: <Widget>[
                                  IconSlideAction(
                                    onTap: () {
                                      setState(() {
                                        FirebaseFirestore.instance
                                            .collection('usersProfile')
                                            .doc(widget.uid)
                                            .collection('notifications')
                                            .doc(snapshot.data.docs[index].id)
                                            .update({'isArchive': true});
                                      });
                                    },
                                    caption: 'Archive',
                                    color: Colors.blue,
                                    icon: Icons.archive,
                                  ),
                                  IconSlideAction(
                                    onTap: () {
                                      if (!snapshot.data.docs[index]
                                          ['isRead']) {
                                        setState(() {
                                          FirebaseFirestore.instance
                                              .collection('usersProfile')
                                              .doc(widget.uid)
                                              .collection('notifications')
                                              .doc(snapshot.data.docs[index].id)
                                              .update({'isRead': true});
                                        });
                                      }
                                    },
                                    caption: 'Mark as read',
                                    color: Colors.indigo,
                                    icon: Icons.visibility,
                                  ),
                                ],
                                secondaryActions: <Widget>[
                                  IconSlideAction(
                                    onTap: () {
                                      setState(() {
                                        FirebaseFirestore.instance
                                            .collection('usersProfile')
                                            .doc(widget.uid)
                                            .collection('notifications')
                                            .doc(snapshot.data.docs[index].id)
                                            .delete();
                                      });
                                    },
                                    caption: 'Delete',
                                    color: Colors.red,
                                    icon: Icons.delete,
                                  ),
                                ],
                              );
                            }),
                      );
                    }
                  } catch (e) {
                    return Center(child: Text(e));
                  }
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        const SizedBox(height: 90),
                        Image.asset(
                          'assets/images/basket.png',
                          scale: 5,
                        ),
                        const SizedBox(height: 20),
                        const Text(
                          'Notifikasi Kosong',
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  );
                }
              }
              return const Center(child: CircularProgressIndicator());
            },
          ),
        ],
      ),
    );
  }
}
