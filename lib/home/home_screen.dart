import 'package:carousel_slider/carousel_slider.dart';
import 'package:cleanis/config/colors.dart';
import 'package:cleanis/config/styles.dart';
import 'package:cleanis/home/widget/location_widget.dart';
import 'package:cleanis/home/widget/title_widget.dart';
import 'package:cleanis/store/store_detail_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:badges/badges.dart';

import 'notifications_screen.dart';
import 'widget/store_widget.dart';
import 'widget/topservice_widget.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Future _getStore;
  final List<String> listOfImage = [];
  var currentUser = FirebaseAuth.instance.currentUser;
  var address = '';
  bool notifications = false;
  int _current = 0;
  final CarouselController _controller = CarouselController();

  @override
  void initState() {
    super.initState();
    getCarousels();
    primaryUserAddress();
    unreadNotification();
    _getStore = getStore();
  }

  unreadNotification() async {
    FirebaseFirestore.instance
        .collection('usersProfile')
        .doc(currentUser.uid)
        .collection('notifications')
        .where('isRead', isEqualTo: false)
        .limit(1)
        .get()
        .then((snapshot) {
      if (snapshot.size == 0) {
        setState(() {
          notifications = false;
        });
      } else {
        setState(() {
          notifications = true;
        });
      }
    });
  }

  primaryUserAddress() async {
    var collection = await FirebaseFirestore.instance
        .collection('usersProfile')
        .doc(currentUser.uid)
        .collection('address')
        .where('primary', isEqualTo: true)
        .get();

    var add = '';

    for (var snapshot in collection.docs) {
      Map<String, dynamic> data = snapshot.data();
      add = data['address'];
    }
    if (mounted) {
      setState(() {
        address = add;
      });
    }
  }

  Future getCarousels() async {
    final carousels =
        await FirebaseFirestore.instance.collection('carousels').get();
    for (var element in carousels.docs) {
      String photoUrl = element.get('photoUrl');
      listOfImage.add(photoUrl);
    }
  }

  Future<QuerySnapshot> getStore() async {
    final result = await FirebaseFirestore.instance
        .collection('stores')
        .orderBy('rate', descending: true)
        .limit(3)
        .get();
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: background,
      appBar: AppBar(
          backgroundColor: primaryColor,
          title: RichText(
              text: TextSpan(
                  text: 'Hi, ',
                  style: const TextStyle(color: Colors.white, fontSize: 16),
                  children: <TextSpan>[
                TextSpan(
                    text: currentUser.displayName, style: appBarStyleLight),
              ])),
          elevation: 0.0,
          actions: <Widget>[
            (notifications)
                ? Badge(
                    position: BadgePosition.topEnd(top: 15, end: 13),
                    child: IconButton(
                        icon: const Icon(Icons.notifications_rounded),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    NotificationsScreen(uid: currentUser.uid)),
                          );
                        }))
                : IconButton(
                    icon: const Icon(Icons.notifications_rounded),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                NotificationsScreen(uid: currentUser.uid)),
                      );
                    })
          ]),
      body: SingleChildScrollView(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          UserLocation(
              location: (address == null)
                  ? 'Loading...'
                  : (address == '')
                      ? 'Kosong'
                      : address),
          CarouselSlider(
            carouselController: _controller,
            options: CarouselOptions(
                autoPlayInterval: const Duration(seconds: 5),
                autoPlayAnimationDuration: const Duration(milliseconds: 1000),
                autoPlay: true,
                enableInfiniteScroll: true,
                initialPage: 1,
                viewportFraction: 0.92,
                disableCenter: true,
                onPageChanged: (index, reason) {
                  setState(() {
                    _current = index;
                  });
                }),
            items: listOfImage
                .map(
                  (item) => Container(
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(8))),
                    margin: const EdgeInsets.fromLTRB(0, 10, 20, 0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8.0),
                      child:
                          Image.network(item, fit: BoxFit.cover, width: 1000),
                    ),
                  ),
                )
                .toList(),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: listOfImage.asMap().entries.map((entry) {
              return GestureDetector(
                onTap: () => _controller.animateToPage(entry.key),
                child: Container(
                  width: 8.0,
                  height: 8.0,
                  margin: const EdgeInsets.fromLTRB(4, 10, 4, 0),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: (Theme.of(context).brightness == Brightness.dark
                            ? Colors.white
                            : primaryColor)
                        .withOpacity(_current == entry.key ? 0.9 : 0.4),
                  ),
                ),
              );
            }).toList(),
          ),
          const TitleSection(title: 'Top Service', subtitle: ''),
          const TopService(),
          const TitleSection(title: 'Popular in Your City', subtitle: ''),
          FutureBuilder(
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                return Container(
                  height: 160,
                  margin: const EdgeInsets.symmetric(
                    vertical: 10,
                    horizontal: 15,
                  ),
                  child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: snapshot.data.docs.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Store(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => StoreDetailScreen(
                                firestoreDocID: snapshot.data.docs[index].id,
                              ),
                            ),
                          );
                        },
                        shopname: snapshot.data.docs[index]['name'] ?? '',
                        address: snapshot.data.docs[index]['address'] ?? '',
                        star: snapshot.data.docs[index]['rate'] ?? '',
                        image: snapshot.data.docs[index]['thumbnail'] ?? '',
                      );
                    },
                  ),
                );
              }
              return const Center(
                child: CircularProgressIndicator(),
              );
            },
            future: _getStore,
          )
        ],
      )),
    );
  }
}
