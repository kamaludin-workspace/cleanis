![banner app](https://storageapi.fleek.co/kamaludin21-team-bucket/cleanis/banner_app.png)

![](https://img.shields.io/badge/version-1.0.1-blue?logo=bitbucket) ![](https://img.shields.io/badge/license-MIT-orange) [![style: lint](https://img.shields.io/badge/style-lint-4BC0F5.svg)](https://pub.dev/packages/lint)
[![Codemagic build status](https://api.codemagic.io/apps/6161172760291a4f3bf38614/6161172760291a4f3bf38613/status_badge.svg)](https://codemagic.io/apps/6161172760291a4f3bf38614/6161172760291a4f3bf38613/latest_build)

## Preview

Cleanis adalah aplikasi servis laundry yang menyediakan fitur on-demand untuk pelayanan jemput antar, dengan fitur peta dan navigasi yang bekerja secara realtime.

## Technologies

 - Flutter (2.5.2)
 - Firebase Storage
 - Firebase Authentications

## System requirements

 - Android OS 
 - Minimum android lolipop


## TODO
 - Near store location map views
 - Scan non-delivery transaction by code/qrcode
 - Search store